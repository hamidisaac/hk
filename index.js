// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
//var io = require('../..')(server);
var io = require('socket.io').listen(server);
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;
var MongoClient = require('mongodb').MongoClient
var db_url = 'mongodb://localhost:27017/mydb';
var db;
var mysql_user = 'root';
//var mysql_pass = '$@m@n64h@m1d70';
var mysql_pass = '123456';
var mysql_db = 'pk';

var hand_score = 1;
var hand_score_negative = 0;

var set_score = 10;
var set_score_negative = -10;

var game_score = 100;
var game_score_negative = -100;

var cot_score = 200;
var cot_score_negative = -200;

var hakem_cot_score = 300;
var hakem_cot_score_negative = -300;

var r = 0;
var mysql = require('mysql')
var md5 = require('md5');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : mysql_user,
    password : mysql_pass,
    database : mysql_db
});

connection.connect();

server.listen(port, function () {
    MongoClient.connect(db_url, function (err, db2) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            db = db2;
            console.log('Connection established to', db_url);
        }
    });
    console.log('Server listening at port %d', port);
});
// Routing
//app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.engine('html', require('ejs').__express);
app.set('view engine', 'html');
app.set('views', __dirname + '/public');
app.use(express.static(__dirname + '/public'));
app.disable('view cache');
app.get('/', function (req, res) {
  //res.send('Illegal Params in get request');
    var url = require('url');
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var user_id = req.query.uid;
    var user_name = req.query.uname;
    r = req.query.room;
    var gl = req.query.gl;
    var si = req.query.si;
    var mode = req.query.mode;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    ip = ip.replace('::ffff:', '');
    if(ips[r] == undefined)
        ips[r] = [];
    if (ips[r].indexOf(ip) == -1 || mode == 'dev') {
        ips[r].push(ip);
        if (user_name == '' || r == '') {
            res.send('Illegal Params in post request');
        } else {

            var sql = 'SELECT * FROM hokm_rooms WHERE id=' + r + ' AND status=1';
            connection.query(sql, function (err, roomr, fields) {
                if (err) {
                    console.log(err);
                }
                if(roomr != undefined) {
                    var time_limit = roomr[0].disconnect_time;
                    var room_name = roomr[0].name;
                    var sql = 'SELECT * FROM users WHERE id=' + user_id + ';';
                    connection.query(sql, function (err, user, fields) {
                        if (err) {
                            console.log(err);
                        }
                        if(user != undefined){
                            if(user[0].id == user_id && user[0].username == user_name && md5(md5(user[0].id+r+user[0].username)) == si || mode == 'dev') {
                                res.render('index.ejs', {
                                    title: 'حکم - ' + room_name,
                                    user_id: user_id,
                                    user_name: user_name,
                                    room: r,
                                    game_limit: gl,
                                    time_limit: time_limit,
                                    si: si
                                });
                            } else {
                                res.render('denied.ejs', {
                                    title: 'access denied',
                                    msg: 'مشکوک میزنی!'
                                });
                            }
                            //connection.end();
                        } else {
                            res.render('denied.ejs', {
                                title: 'access denied',
                                msg: 'تو کی هستی؟'
                            });
                        }
                    })
                } else {
                    res.render('denied.ejs', {
                        title: 'access denied',
                        msg: 'این بازی وجود ندارد'
                    });
                }
            })
        }
    } else {
        res.render('denied.ejs', {
            title: 'access denied',
            msg: 'شما مجوز ورود به این صفحه را ندارید'
        });
    }
});
var ips = [];
app.post('/', function(req, res) {
    var user_id = req.body.uid;
    var user_name = req.body.uname;
    r = req.body.room;
    var gl = req.body.gl;
    var si = req.query.si;
    var mode = req.body.mode;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    ip = ip.replace('::ffff:', '');
    if(ips[r] == undefined)
        ips[r] = [];
    if (ips[r].indexOf(ip) == -1 || mode == 'dev') {
        ips[r].push(ip);
        if (user_name == '' || r == '') {
            res.send('Illegal Params in post request');
        } else {
            var sql = 'SELECT * FROM hokm_rooms WHERE id=' + r + ' AND status=1';
            connection.query(sql, function (err, roomr, fields) {
                if (err) {
                    console.log(err);
                }
                if(roomr != undefined) {
                    var time_limit = roomr[0].disconnect_time;
                    var room_name = roomr[0].name;
                    var sql = 'SELECT * FROM users WHERE id=' + user_id + ';';
                    connection.query(sql, function (err, user, fields) {
                        if (err) {
                            console.log(err);
                        }
                        console.log(user);
                        if(user[0] != undefined){
                            if(user[0].id == user_id && user[0].username == user_name && md5(md5(user[0].id+r+user[0].username)) == si || mode == 'dev') {
                                res.render('index.ejs', {
                                    title: 'حکم - ' + room_name,
                                    user_id: user_id,
                                    user_name: user_name,
                                    room: r,
                                    game_limit: gl,
                                    time_limit: time_limit,
                                    si: si
                                });
                            } else {
                                res.render('denied.ejs', {
                                    title: 'access denied',
                                    msg: 'مشکوک میزنی!'
                                });
                            }
                            //connection.end();
                        } else {
                            res.render('denied.ejs', {
                                title: 'access denied',
                                msg: 'تو کی هستی؟'
                            });
                        }
                    })
                } else {
                    res.render('denied.ejs', {
                        title: 'access denied',
                        msg: 'این بازی وجود ندارد'
                    });
                }
            })
        }
    } else {
        res.render('denied.ejs', {
            title: 'access denied',
            msg: 'شما مجوز ورود به این صفحه را ندارید'
        });
    }
});

function setHands(room, callback)
{
  while(io.sockets.adapter.rooms[room].taeenHakemRnd.length < 52) {
    rnd = Math.floor(Math.random() * 52) + 1;
    if (io.sockets.adapter.rooms[room].taeenHakemRnd.indexOf(rnd) == -1) {
      io.sockets.adapter.rooms[room].taeenHakemRnd.push(rnd);
    }
  }
  while(io.sockets.adapter.rooms[room].dastRnd.length < 52) {
    rnd = Math.floor(Math.random() * 52) + 1;
    if (io.sockets.adapter.rooms[room].dastRnd.indexOf(rnd) == -1) {
      io.sockets.adapter.rooms[room].dastRnd.push(rnd);
    }
  }

  for(i=0;i<52;i++){
    if(io.sockets.adapter.rooms[room].hakem == '' || io.sockets.adapter.rooms[room].yarHakem == '') {
      if (io.sockets.adapter.rooms[room].hakem == '') {
        if (io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 13 || io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 26 || io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 39 || io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 52) {
          io.sockets.adapter.rooms[room].hakem_code = Math.floor(Math.random() * 4);
          io.sockets.adapter.rooms[room].hakem = io.sockets.adapter.rooms[room].users[io.sockets.adapter.rooms[room].hakem_code];
        }
      } else {
        if (io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 13 || io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 26 || io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 39 || io.sockets.adapter.rooms[room].taeenHakemRnd[i] == 52) {
          io.sockets.adapter.rooms[room].yarhakem_code = Math.floor(Math.random() * 4);
          if(io.sockets.adapter.rooms[room].yarhakem_code == io.sockets.adapter.rooms[room].hakem_code)
            io.sockets.adapter.rooms[room].yarhakem_code = Math.floor(Math.random() * 4);
          if(io.sockets.adapter.rooms[room].yarhakem_code == io.sockets.adapter.rooms[room].hakem_code)
            io.sockets.adapter.rooms[room].yarhakem_code = Math.floor(Math.random() * 4);
          if(io.sockets.adapter.rooms[room].yarhakem_code == io.sockets.adapter.rooms[room].hakem_code)
            io.sockets.adapter.rooms[room].yarhakem_code = Math.floor(Math.random() * 4);
          io.sockets.adapter.rooms[room].yarHakem = io.sockets.adapter.rooms[room].users[io.sockets.adapter.rooms[room].yarhakem_code];
        }
      }
    }
  }
  io.sockets.adapter.rooms[room].user1 = io.sockets.adapter.rooms[room].hakem;
  io.sockets.adapter.rooms[room].user3 = io.sockets.adapter.rooms[room].yarHakem;
  if(io.sockets.adapter.rooms[room].hakem_code == 0 && io.sockets.adapter.rooms[room].yarhakem_code == 1){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[2];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[3];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 0 && io.sockets.adapter.rooms[room].yarhakem_code == 2){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[1];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[3];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 0 && io.sockets.adapter.rooms[room].yarhakem_code == 3){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[1];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[2];
  }

  if(io.sockets.adapter.rooms[room].hakem_code == 1 && io.sockets.adapter.rooms[room].yarhakem_code == 0){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[2];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[3];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 1 && io.sockets.adapter.rooms[room].yarhakem_code == 2){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[0];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[3];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 1 && io.sockets.adapter.rooms[room].yarhakem_code == 3){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[0];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[2];
  }

  if(io.sockets.adapter.rooms[room].hakem_code == 2 && io.sockets.adapter.rooms[room].yarhakem_code == 0){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[1];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[3];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 2 && io.sockets.adapter.rooms[room].yarhakem_code == 1){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[0];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[3];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 2 && io.sockets.adapter.rooms[room].yarhakem_code == 3){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[0];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[1];
  }

  if(io.sockets.adapter.rooms[room].hakem_code == 3 && io.sockets.adapter.rooms[room].yarhakem_code == 0){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[1];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[2];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 3 && io.sockets.adapter.rooms[room].yarhakem_code == 1){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[0];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[2];
  }
  if(io.sockets.adapter.rooms[room].hakem_code == 3 && io.sockets.adapter.rooms[room].yarhakem_code == 2){
    io.sockets.adapter.rooms[room].user2 = io.sockets.adapter.rooms[room].users[0];
    io.sockets.adapter.rooms[room].user4 = io.sockets.adapter.rooms[room].users[1];
  }

  console.log('team 1 : ' + io.sockets.adapter.rooms[room].user1 + ' & ' + io.sockets.adapter.rooms[room].user3);
  console.log('team 2 : ' + io.sockets.adapter.rooms[room].user2 + ' & ' + io.sockets.adapter.rooms[room].user4);
  callback(null,true);
}

function get52RndCards(room, callback)
{
  while(io.sockets.adapter.rooms[room].dastRnd.length < 52) {
    rnd = Math.floor(Math.random() * 52) + 1;
    if (io.sockets.adapter.rooms[room].dastRnd.indexOf(rnd) == -1) {
      io.sockets.adapter.rooms[room].dastRnd.push(rnd);
    }
  }
  callback(null,true);
}

io.on('connection', function (socket) {
  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
      if(socket.username != undefined) {
          var from = socket.username;
          var ack_flag = true;
          if(data == 'ack'){
              var index = io.sockets.adapter.rooms[socket.room].disconnecteds.indexOf(io.sockets.adapter.rooms[socket.room].nobat);
              if (index > -1) {
                  ack_flag = true;
                  username = io.sockets.adapter.rooms[socket.room].nobat;
                  from = io.sockets.adapter.rooms[socket.room].nobat;
                  if(io.sockets.adapter.rooms[socket.room].hakem == io.sockets.adapter.rooms[socket.room].nobat && io.sockets.adapter.rooms[socket.room].hokm == ''){
                      //select hokm auto
                      var h = 0;
                      var s = 0;
                      var d = 0;
                      var c = 0;
                      var hw = 0;
                      var sw = 0;
                      var dw = 0;
                      var cw = 0;
                      for (i = 0; i <= 4; i++) {
                          if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] >= 1 && io.sockets.adapter.rooms[socket.room].usersHands[username][i] <= 13) {
                              h++;
                              hw += io.sockets.adapter.rooms[socket.room].usersHands[username][i];
                          }
                          if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] >= 14 && io.sockets.adapter.rooms[socket.room].usersHands[username][i] <= 26) {
                              s++;
                              sw += io.sockets.adapter.rooms[socket.room].usersHands[username][i] - 13;
                          }
                          if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] >= 27 && io.sockets.adapter.rooms[socket.room].usersHands[username][i] <= 39) {
                              d++;
                              dw += io.sockets.adapter.rooms[socket.room].usersHands[username][i] - 26;
                          }
                          if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] >= 40 && io.sockets.adapter.rooms[socket.room].usersHands[username][i] <= 52) {
                              c++;
                              cw += io.sockets.adapter.rooms[socket.room].usersHands[username][i] - 39;
                          }
                      }
                      var maxh = h;
                      var maxhw = hw;
                      data = 'h:h';
                      if ((s > maxh) || (s == maxh && sw > maxhw)) {
                          data = 'h:s';
                          maxh = s;
                          maxhw = sw;
                      }
                      if ((d > maxh) || (d == maxh && dw > maxhw)) {
                          data = 'h:d';
                          maxh = d;
                          maxhw = dw;
                      }
                      if ((c > maxh) || (c == maxh && cw > maxhw)) {
                          data = 'h:c';
                          maxh = c;
                          maxhw = cw;
                      }
                  } else {
                      //new move auto
                      if (io.sockets.adapter.rooms[socket.room].zamin == '') {
                          var s = [];
                          var s2 = [];
                          for (i = 0; i <= io.sockets.adapter.rooms[socket.room].usersHands[username].length; i++) {
                              if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 13 ||
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 26 ||
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 39 ||
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 52
                              ) {
                                  s.push(io.sockets.adapter.rooms[socket.room].usersHands[username][i]);
                              }
                              if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 12 ||
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 25 ||
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 38 ||
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][i] == 51
                              ) {
                                  s2.push(io.sockets.adapter.rooms[socket.room].usersHands[username][i]);
                              }
                          }
                          if (s.length > 0) {
                              rnd = Math.floor(Math.random() * s.length);
                              data = 'm:'+s[rnd];
                          } else if (s2.length > 0 && io.sockets.adapter.rooms[socket.room].usersHands[username].length <= 9) {
                              rnd = Math.floor(Math.random() * s2.length);
                              data = 'm:'+s2[rnd];
                          } else {
                              rnd = Math.floor(Math.random() * io.sockets.adapter.rooms[socket.room].usersHands[username].length);
                              data = 'm:'+io.sockets.adapter.rooms[socket.room].usersHands[username][rnd];
                          }
                      } else {
                          var j = [];
                          for (i = 0; i <= io.sockets.adapter.rooms[socket.room].usersHands[username].length; i++) {
                              if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] >= io.sockets.adapter.rooms[socket.room].minzamin && io.sockets.adapter.rooms[socket.room].usersHands[username][i] <= io.sockets.adapter.rooms[socket.room].maxzamin) {
                                  j.push(io.sockets.adapter.rooms[socket.room].usersHands[username][i]);
                              }
                          }
                          if (j.length > 0) {
                              j.sort(function (a, b) {
                                  return b - a
                              });
                              if (data.step == 2) {
                                  data = 'm:'+j[0];
                              } else {
                                  data = 'm:'+j[j.length - 1];
                              }
                          } else {
                              var h = [];
                              for (i = 0; i <= io.sockets.adapter.rooms[socket.room].usersHands[username].length; i++) {
                                  if (io.sockets.adapter.rooms[socket.room].usersHands[username][i] >= io.sockets.adapter.rooms[socket.room].minhokm && io.sockets.adapter.rooms[socket.room].usersHands[username][i] <= io.sockets.adapter.rooms[socket.room].maxhokm) {
                                      h.push(io.sockets.adapter.rooms[socket.room].usersHands[username][i]);
                                  }
                              }
                              if (h.length > 0) {
                                  h.sort(function (a, b) {
                                      return b - a
                                  });
                                  if (data.step == 2) {
                                      data = 'm:'+h[0];
                                  } else {
                                      data = 'm:'+h[h.length - 1];
                                  }
                              } else {
                                  rnd = Math.floor(Math.random() * io.sockets.adapter.rooms[socket.room].usersHands[username].length);
                                  data = 'm:'+io.sockets.adapter.rooms[socket.room].usersHands[username][rnd];
                              }
                          }
                      }
                  }
              } else {
                  ack_flag = false;
              }
          }
          if(ack_flag) {
              console.log('new msg ' + from + ' =>' + data);
              if (io.sockets.adapter.rooms[socket.room].gamewinner != '' && io.sockets.adapter.rooms[socket.room].gamewinner != '0') {
                  io.sockets.in(socket.room).emit('game finish', {
                      user1: io.sockets.adapter.rooms[socket.room].user1,
                      user2: io.sockets.adapter.rooms[socket.room].user2,
                      user3: io.sockets.adapter.rooms[socket.room].user3,
                      user4: io.sockets.adapter.rooms[socket.room].user4,
                      user1score: io.sockets.adapter.rooms[socket.room].user1win,
                      user2score: io.sockets.adapter.rooms[socket.room].user2win,
                      user3score: io.sockets.adapter.rooms[socket.room].user3win,
                      user4score: io.sockets.adapter.rooms[socket.room].user4win,
                      gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
                      setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
                      setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24
                  });
                  console.log('game finished - winner : ' + io.sockets.adapter.rooms[socket.room].gamewinner);
              } else {
                  // we tell the client to execute 'new message'
                  if (data.substring(0, 2) == 'm:') {
                      if (io.sockets.adapter.rooms[socket.room].hokm != undefined && io.sockets.adapter.rooms[socket.room].hokm != '') {
                          if (from == io.sockets.adapter.rooms[socket.room].nobat) {
                              io.sockets.adapter.rooms[socket.room].m = parseInt(data.toString().split(':')[1]);
                              if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(io.sockets.adapter.rooms[socket.room].m) == -1) {
                                  console.log('barge ' + data.toString().split(':')[1] + ' daste shoma nist : ' + from);
                              } else {
                                  io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                  io.sockets.adapter.rooms[socket.room].dare = false;
                                  console.log('zamin : ' + io.sockets.adapter.rooms[socket.room].zamin);
                                  if (io.sockets.adapter.rooms[socket.room].zamin != '') {
                                      io.sockets.adapter.rooms[socket.room].kosbazi = true;
                                      switch (io.sockets.adapter.rooms[socket.room].zamin) {
                                          case 'hearts':
                                              if (io.sockets.adapter.rooms[socket.room].m >= 1 && io.sockets.adapter.rooms[socket.room].m <= 13) {
                                                  io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              } else {
                                                  io.sockets.adapter.rooms[socket.room].del_dare = false;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(1) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(2) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(3) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(4) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(5) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(6) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(7) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(8) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(9) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(10) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(11) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(12) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(13) != -1)
                                                      io.sockets.adapter.rooms[socket.room].del_dare = true;
                                                  if ((io.sockets.adapter.rooms[socket.room].m < 1 || io.sockets.adapter.rooms[socket.room].m > 13) && io.sockets.adapter.rooms[socket.room].del_dare)
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = true;
                                                  if (!io.sockets.adapter.rooms[socket.room].del_dare && (io.sockets.adapter.rooms[socket.room].m < 1 || io.sockets.adapter.rooms[socket.room].m > 13))
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              }
                                              console.log('hearts', io.sockets.adapter.rooms[socket.room].m, io.sockets.adapter.rooms[socket.room].del_dare, io.sockets.adapter.rooms[socket.room].kosbazi);
                                              break;

                                          case 'spades':
                                              if (io.sockets.adapter.rooms[socket.room].m >= 14 && io.sockets.adapter.rooms[socket.room].m <= 26) {
                                                  io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              } else {
                                                  io.sockets.adapter.rooms[socket.room].pik_dare = false;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(14) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(15) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(16) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(17) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(18) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(19) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(20) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(21) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(22) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(23) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(24) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(25) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(26) != -1)
                                                      io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                                  if ((io.sockets.adapter.rooms[socket.room].m < 14 || io.sockets.adapter.rooms[socket.room].m > 26) && io.sockets.adapter.rooms[socket.room].pik_dare)
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = true;
                                                  if (!io.sockets.adapter.rooms[socket.room].pik_dare && (io.sockets.adapter.rooms[socket.room].m < 14 || io.sockets.adapter.rooms[socket.room].m > 26))
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              }
                                              console.log('spades', io.sockets.adapter.rooms[socket.room].m, io.sockets.adapter.rooms[socket.room].pik_dare, io.sockets.adapter.rooms[socket.room].kosbazi);
                                              break;

                                          case 'diamonds':
                                              if (io.sockets.adapter.rooms[socket.room].m >= 27 && io.sockets.adapter.rooms[socket.room].m <= 39) {
                                                  io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              } else {
                                                  io.sockets.adapter.rooms[socket.room].khesht_dare = false;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(27) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(28) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(29) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(30) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(31) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(32) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(33) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(34) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(35) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(36) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(37) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(38) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(39) != -1)
                                                      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                                  if ((io.sockets.adapter.rooms[socket.room].m < 27 || io.sockets.adapter.rooms[socket.room].m > 39) && io.sockets.adapter.rooms[socket.room].khesht_dare)
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = true;
                                                  if (!io.sockets.adapter.rooms[socket.room].khesht_dare && (io.sockets.adapter.rooms[socket.room].m < 27 || io.sockets.adapter.rooms[socket.room].m > 39))
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              }
                                              console.log('diamonds', io.sockets.adapter.rooms[socket.room].m, io.sockets.adapter.rooms[socket.room].khesht_dare, io.sockets.adapter.rooms[socket.room].kosbazi);
                                              break;

                                          case 'clubs':
                                              if (io.sockets.adapter.rooms[socket.room].m >= 40 && io.sockets.adapter.rooms[socket.room].m <= 52) {
                                                  io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              } else {
                                                  io.sockets.adapter.rooms[socket.room].gish_dare = false;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(40) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(41) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(42) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(43) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(44) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(45) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(46) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(47) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(48) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(49) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(50) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(51) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if (io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(52) != -1)
                                                      io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                                  if ((io.sockets.adapter.rooms[socket.room].m < 40 || io.sockets.adapter.rooms[socket.room].m > 52) && io.sockets.adapter.rooms[socket.room].gish_dare)
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = true;
                                                  if (!io.sockets.adapter.rooms[socket.room].gish_dare && (io.sockets.adapter.rooms[socket.room].m < 40 || io.sockets.adapter.rooms[socket.room].m > 52))
                                                      io.sockets.adapter.rooms[socket.room].kosbazi = false;
                                              }
                                              console.log('clubs', io.sockets.adapter.rooms[socket.room].m, io.sockets.adapter.rooms[socket.room].gish_dare, io.sockets.adapter.rooms[socket.room].kosbazi);
                                              break;
                                      }
                                  }
                                  if (io.sockets.adapter.rooms[socket.room].kosbazi) {
                                      console.log(from + ' dorost bazi kon!');
                                      io.sockets.connected[socket.id].emit('new message', {
                                          username: from,
                                          message: 'درست بازی کن!'
                                      });
                                  } else {
                                      if (io.sockets.adapter.rooms[socket.room].m1 == 0 && io.sockets.adapter.rooms[socket.room].m2 == 0 && io.sockets.adapter.rooms[socket.room].m3 == 0 && io.sockets.adapter.rooms[socket.room].m4 == 0) {
                                          if (io.sockets.adapter.rooms[socket.room].m >= 1 && io.sockets.adapter.rooms[socket.room].m <= 13) {
                                              io.sockets.adapter.rooms[socket.room].zamin = 'hearts';
                                              io.sockets.adapter.rooms[socket.room].minzamin = 1;
                                              io.sockets.adapter.rooms[socket.room].maxzamin = 13;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m >= 14 && io.sockets.adapter.rooms[socket.room].m <= 26) {
                                              io.sockets.adapter.rooms[socket.room].zamin = 'spades';
                                              io.sockets.adapter.rooms[socket.room].minzamin = 14;
                                              io.sockets.adapter.rooms[socket.room].maxzamin = 26;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m >= 27 && io.sockets.adapter.rooms[socket.room].m <= 39) {
                                              io.sockets.adapter.rooms[socket.room].zamin = 'diamonds';
                                              io.sockets.adapter.rooms[socket.room].minzamin = 27;
                                              io.sockets.adapter.rooms[socket.room].maxzamin = 39;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m >= 40 && io.sockets.adapter.rooms[socket.room].m <= 52) {
                                              io.sockets.adapter.rooms[socket.room].zamin = 'clubs';
                                              io.sockets.adapter.rooms[socket.room].minzamin = 40;
                                              io.sockets.adapter.rooms[socket.room].maxzamin = 52;
                                          }
                                      }
                                      if (io.sockets.adapter.rooms[socket.room].m1 == 0 && io.sockets.adapter.rooms[socket.room].user1 == from) {
                                          io.sockets.adapter.rooms[socket.room].m1 = parseInt(data.toString().split(':')[1]);
                                      }
                                      if (io.sockets.adapter.rooms[socket.room].m2 == 0 && io.sockets.adapter.rooms[socket.room].user2 == from) {
                                          io.sockets.adapter.rooms[socket.room].m2 = parseInt(data.toString().split(':')[1]);
                                      }
                                      if (io.sockets.adapter.rooms[socket.room].m3 == 0 && io.sockets.adapter.rooms[socket.room].user3 == from) {
                                          io.sockets.adapter.rooms[socket.room].m3 = parseInt(data.toString().split(':')[1]);
                                      }
                                      if (io.sockets.adapter.rooms[socket.room].m4 == 0 && io.sockets.adapter.rooms[socket.room].user4 == from) {
                                          io.sockets.adapter.rooms[socket.room].m4 = parseInt(data.toString().split(':')[1]);
                                      }
                                      var index = io.sockets.adapter.rooms[socket.room].usersHands[from].indexOf(io.sockets.adapter.rooms[socket.room].m);
                                      if (index > -1) {
                                          io.sockets.adapter.rooms[socket.room].usersHands[from].splice(index, 1);
                                      }
                                      switch (io.sockets.adapter.rooms[socket.room].nobat) {
                                          case io.sockets.adapter.rooms[socket.room].user1:
                                              io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user2;
                                              break;
                                          case io.sockets.adapter.rooms[socket.room].user2:
                                              io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user3;
                                              break;
                                          case io.sockets.adapter.rooms[socket.room].user3:
                                              io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user4;
                                              break;
                                          case io.sockets.adapter.rooms[socket.room].user4:
                                              io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user1;
                                              break;
                                      }
                                      if (io.sockets.adapter.rooms[socket.room].m1 != 0 && io.sockets.adapter.rooms[socket.room].m2 != 0 && io.sockets.adapter.rooms[socket.room].m3 != 0 && io.sockets.adapter.rooms[socket.room].m4 != 0) {
                                          switch (io.sockets.adapter.rooms[socket.room].hokm) {
                                              case 'hearts':
                                                  io.sockets.adapter.rooms[socket.room].minhokm = 1;
                                                  io.sockets.adapter.rooms[socket.room].maxhokm = 13;
                                                  break;
                                              case 'spades':
                                                  io.sockets.adapter.rooms[socket.room].minhokm = 14;
                                                  io.sockets.adapter.rooms[socket.room].maxhokm = 26;
                                                  break;
                                              case 'diamonds':
                                                  io.sockets.adapter.rooms[socket.room].minhokm = 27;
                                                  io.sockets.adapter.rooms[socket.room].maxhokm = 39;
                                                  break;
                                              case 'clubs':
                                                  io.sockets.adapter.rooms[socket.room].minhokm = 40;
                                                  io.sockets.adapter.rooms[socket.room].maxhokm = 52;
                                                  break;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m1 >= 1 && io.sockets.adapter.rooms[socket.room].m1 <= 13) {
                                              io.sockets.adapter.rooms[socket.room].min = 1;
                                              io.sockets.adapter.rooms[socket.room].max = 13;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m1 >= 14 && io.sockets.adapter.rooms[socket.room].m1 <= 26) {
                                              io.sockets.adapter.rooms[socket.room].min = 14;
                                              io.sockets.adapter.rooms[socket.room].max = 26;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m1 >= 27 && io.sockets.adapter.rooms[socket.room].m1 <= 39) {
                                              io.sockets.adapter.rooms[socket.room].min = 27;
                                              io.sockets.adapter.rooms[socket.room].max = 39;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].m1 >= 40 && io.sockets.adapter.rooms[socket.room].m1 <= 52) {
                                              io.sockets.adapter.rooms[socket.room].min = 40;
                                              io.sockets.adapter.rooms[socket.room].max = 52;
                                          }
                                          io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user1;
                                          io.sockets.adapter.rooms[socket.room].sar = 0;
                                          io.sockets.adapter.rooms[socket.room].boresh = 0;
                                          if (io.sockets.adapter.rooms[socket.room].m1 >= io.sockets.adapter.rooms[socket.room].minzamin && io.sockets.adapter.rooms[socket.room].m1 <= io.sockets.adapter.rooms[socket.room].maxzamin)
                                              io.sockets.adapter.rooms[socket.room].sar = io.sockets.adapter.rooms[socket.room].m1;
                                          if (io.sockets.adapter.rooms[socket.room].m1 >= io.sockets.adapter.rooms[socket.room].minhokm && io.sockets.adapter.rooms[socket.room].m1 <= io.sockets.adapter.rooms[socket.room].maxhokm)
                                              io.sockets.adapter.rooms[socket.room].boresh = io.sockets.adapter.rooms[socket.room].m1;
                                          if ((io.sockets.adapter.rooms[socket.room].m2 >= io.sockets.adapter.rooms[socket.room].minzamin && io.sockets.adapter.rooms[socket.room].m2 <= io.sockets.adapter.rooms[socket.room].maxzamin) && (io.sockets.adapter.rooms[socket.room].boresh == 0)) {
                                              if (io.sockets.adapter.rooms[socket.room].m2 > io.sockets.adapter.rooms[socket.room].sar) {
                                                  io.sockets.adapter.rooms[socket.room].sar = io.sockets.adapter.rooms[socket.room].m2;
                                                  io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user2;
                                              }
                                          } else {
                                              //ya boride ya rad dade
                                              if (io.sockets.adapter.rooms[socket.room].m2 >= io.sockets.adapter.rooms[socket.room].minhokm && io.sockets.adapter.rooms[socket.room].m2 <= io.sockets.adapter.rooms[socket.room].maxhokm) {
                                                  if (io.sockets.adapter.rooms[socket.room].m2 > io.sockets.adapter.rooms[socket.room].boresh) {
                                                      io.sockets.adapter.rooms[socket.room].boresh = io.sockets.adapter.rooms[socket.room].m2;
                                                      io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user2;
                                                  }
                                              }
                                          }

                                          if ((io.sockets.adapter.rooms[socket.room].m3 >= io.sockets.adapter.rooms[socket.room].minzamin && io.sockets.adapter.rooms[socket.room].m3 <= io.sockets.adapter.rooms[socket.room].maxzamin) && (io.sockets.adapter.rooms[socket.room].boresh == 0)) {
                                              if (io.sockets.adapter.rooms[socket.room].m3 > io.sockets.adapter.rooms[socket.room].sar) {
                                                  io.sockets.adapter.rooms[socket.room].sar = io.sockets.adapter.rooms[socket.room].m3;
                                                  io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user3;
                                              }
                                          } else {
                                              //ya boride ya rad dade
                                              if (io.sockets.adapter.rooms[socket.room].m3 >= io.sockets.adapter.rooms[socket.room].minhokm && io.sockets.adapter.rooms[socket.room].m3 <= io.sockets.adapter.rooms[socket.room].maxhokm) {
                                                  if (io.sockets.adapter.rooms[socket.room].m3 > io.sockets.adapter.rooms[socket.room].boresh) {
                                                      io.sockets.adapter.rooms[socket.room].boresh = io.sockets.adapter.rooms[socket.room].m3;
                                                      io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user3;
                                                  }
                                              }
                                          }

                                          if ((io.sockets.adapter.rooms[socket.room].m4 >= io.sockets.adapter.rooms[socket.room].minzamin && io.sockets.adapter.rooms[socket.room].m4 <= io.sockets.adapter.rooms[socket.room].maxzamin) && (io.sockets.adapter.rooms[socket.room].boresh == 0)) {
                                              if (io.sockets.adapter.rooms[socket.room].m4 > io.sockets.adapter.rooms[socket.room].sar) {
                                                  io.sockets.adapter.rooms[socket.room].sar = io.sockets.adapter.rooms[socket.room].m4;
                                                  io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user4;
                                              }
                                          } else {
                                              //ya boride ya rad dade
                                              if (io.sockets.adapter.rooms[socket.room].m4 >= io.sockets.adapter.rooms[socket.room].minhokm && io.sockets.adapter.rooms[socket.room].m4 <= io.sockets.adapter.rooms[socket.room].maxhokm) {
                                                  if (io.sockets.adapter.rooms[socket.room].m4 > io.sockets.adapter.rooms[socket.room].boresh) {
                                                      io.sockets.adapter.rooms[socket.room].boresh = io.sockets.adapter.rooms[socket.room].m4;
                                                      io.sockets.adapter.rooms[socket.room].winner = io.sockets.adapter.rooms[socket.room].user4;
                                                  }
                                              }
                                          }
                                          switch (io.sockets.adapter.rooms[socket.room].winner) {
                                              case io.sockets.adapter.rooms[socket.room].user1:
                                                  //set user hand ex 1 & 3
                                                  io.sockets.adapter.rooms[socket.room].user1win++;
                                                  io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user1;
                                                  break;
                                              case io.sockets.adapter.rooms[socket.room].user2:
                                                  //set user hand ex 2 & 4
                                                  io.sockets.adapter.rooms[socket.room].user2win++;
                                                  io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user2;
                                                  break;
                                              case io.sockets.adapter.rooms[socket.room].user3:
                                                  //set user hand ex 1 & 3
                                                  io.sockets.adapter.rooms[socket.room].user3win++;
                                                  io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user3;
                                                  break;
                                              case io.sockets.adapter.rooms[socket.room].user4:
                                                  //set user hand ex 2 & 4
                                                  io.sockets.adapter.rooms[socket.room].user4win++;
                                                  io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user4;
                                                  break;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win == 7) {
                                              console.log('hand winner : ' + io.sockets.adapter.rooms[socket.room].user1 + ' : ' + io.sockets.adapter.rooms[socket.room].user1win + ' & ' + io.sockets.adapter.rooms[socket.room].user3 + ' : ' + io.sockets.adapter.rooms[socket.room].user3win);
                                              io.sockets.adapter.rooms[socket.room].user1win = 0;
                                              io.sockets.adapter.rooms[socket.room].user2win = 0;
                                              io.sockets.adapter.rooms[socket.room].user3win = 0;
                                              io.sockets.adapter.rooms[socket.room].user4win = 0;
                                              io.sockets.adapter.rooms[socket.room].setwinner13++;
                                              //set user set ex 1 & 3 and negative ex 2 & 4
                                              if (io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win == 7 && io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win == 0) {
                                                  io.sockets.adapter.rooms[socket.room].setwinner13++;
                                                  //cot
                                                  //set user set cot ex 1 & 3 and negative ex 2 & 4
                                                  if (io.sockets.adapter.rooms[socket.room].hakem == io.sockets.adapter.rooms[socket.room].user2 || io.sockets.adapter.rooms[socket.room].hakem == io.sockets.adapter.rooms[socket.room].user4) {
                                                      // hakem cot
                                                      //set user hakem cot ex 1 & 3 and negative ex 2 & 4
                                                      io.sockets.adapter.rooms[socket.room].setwinner13++;
                                                  }
                                              }

                                              switch (io.sockets.adapter.rooms[socket.room].hakem) {
                                                  case io.sockets.adapter.rooms[socket.room].user1:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user1;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user1;
                                                      break;
                                                  case io.sockets.adapter.rooms[socket.room].user2:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user3;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user3;
                                                      break;
                                                  case io.sockets.adapter.rooms[socket.room].user3:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user3;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user3;
                                                      break;
                                                  case io.sockets.adapter.rooms[socket.room].user4:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user1;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user1;
                                                      break;
                                              }

                                              if (io.sockets.adapter.rooms[socket.room].setwinner13 < io.sockets.adapter.rooms[socket.room].game_limit) {
                                                  io.sockets.adapter.rooms[socket.room].gamewinner = '';
                                              } else if (io.sockets.adapter.rooms[socket.room].setwinner13 == io.sockets.adapter.rooms[socket.room].game_limit) {
                                                  io.sockets.adapter.rooms[socket.room].gamewinner = io.sockets.adapter.rooms[socket.room].user1 + ',' + io.sockets.adapter.rooms[socket.room].user3;
                                                  console.log('game winner : ' + io.sockets.adapter.rooms[socket.room].user1 + ' & ' + io.sockets.adapter.rooms[socket.room].user3);
                                              }
                                              /*io.sockets.adapter.rooms[socket.room].user1win = 0;
                                               io.sockets.adapter.rooms[socket.room].user2win = 0;
                                               io.sockets.adapter.rooms[socket.room].user3win = 0;
                                               io.sockets.adapter.rooms[socket.room].user4win = 0;*/
                                          } else if (io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win == 7) {
                                              console.log('hand winner : ' + io.sockets.adapter.rooms[socket.room].user2 + ' : ' + io.sockets.adapter.rooms[socket.room].user2win + ' & ' + io.sockets.adapter.rooms[socket.room].user4 + ' : ' + io.sockets.adapter.rooms[socket.room].user4win);
                                              io.sockets.adapter.rooms[socket.room].user1win = 0;
                                              io.sockets.adapter.rooms[socket.room].user2win = 0;
                                              io.sockets.adapter.rooms[socket.room].user3win = 0;
                                              io.sockets.adapter.rooms[socket.room].user4win = 0;
                                              io.sockets.adapter.rooms[socket.room].setwinner24++;
                                              //set user set ex 2 & 4 and negative ex 1 & 3
                                              if (io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win == 7 && io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win == 0) {
                                                  io.sockets.adapter.rooms[socket.room].setwinner24++;
                                                  //cot
                                                  //set user cot ex 2 & 4 and negative ex 1 & 3
                                                  if (io.sockets.adapter.rooms[socket.room].hakem == io.sockets.adapter.rooms[socket.room].user1 || io.sockets.adapter.rooms[socket.room].hakem == io.sockets.adapter.rooms[socket.room].user3) {
                                                      // hakem cot
                                                      //set user hakem cot ex 2 & 4 and negative ex 1 & 3
                                                      io.sockets.adapter.rooms[socket.room].setwinner24++;
                                                  }
                                              }

                                              switch (io.sockets.adapter.rooms[socket.room].hakem) {
                                                  case io.sockets.adapter.rooms[socket.room].user1:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user2;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user2;
                                                      break;
                                                  case io.sockets.adapter.rooms[socket.room].user2:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user2;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user2;
                                                      break;
                                                  case io.sockets.adapter.rooms[socket.room].user3:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user4;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user4;
                                                      break;
                                                  case io.sockets.adapter.rooms[socket.room].user4:
                                                      io.sockets.adapter.rooms[socket.room].hakem = io.sockets.adapter.rooms[socket.room].user4;
                                                      io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].user4;
                                                      break;
                                              }

                                              if (io.sockets.adapter.rooms[socket.room].setwinner24 < io.sockets.adapter.rooms[socket.room].game_limit) {
                                                  io.sockets.adapter.rooms[socket.room].gamewinner = '';
                                                  console.log('game winner : ' + io.sockets.adapter.rooms[socket.room].user2 + ' & ' + io.sockets.adapter.rooms[socket.room].user4);
                                              } else if (io.sockets.adapter.rooms[socket.room].setwinner24 == io.sockets.adapter.rooms[socket.room].game_limit) {
                                                  io.sockets.adapter.rooms[socket.room].gamewinner = io.sockets.adapter.rooms[socket.room].user2 + ',' + io.sockets.adapter.rooms[socket.room].user4;
                                              }
                                              /*io.sockets.adapter.rooms[socket.room].user1win = 0;
                                               io.sockets.adapter.rooms[socket.room].user2win = 0;
                                               io.sockets.adapter.rooms[socket.room].user3win = 0;
                                               io.sockets.adapter.rooms[socket.room].user4win = 0;*/
                                          } else {
                                              io.sockets.adapter.rooms[socket.room].handflag = true;
                                          }
                                          if (io.sockets.adapter.rooms[socket.room].gamewinner == '') {
                                              io.sockets.adapter.rooms[socket.room].hokm = '';
                                              io.sockets.adapter.rooms[socket.room].zamin = '';
                                              io.sockets.adapter.rooms[socket.room].dastRnd = [];
                                              get52RndCards(socket.room, function (err) {
                                                  if (err) {
                                                      // error handling code goes here
                                                      console.log("ERROR : ", err);
                                                  } else {
                                                      io.sockets.adapter.rooms[socket.room].step = 0;
                                                      io.sockets.adapter.rooms[socket.room].m1 = 0;
                                                      io.sockets.adapter.rooms[socket.room].m2 = 0;
                                                      io.sockets.adapter.rooms[socket.room].m3 = 0;
                                                      io.sockets.adapter.rooms[socket.room].m4 = 0;
                                                      io.sockets.adapter.rooms[socket.room].user1win = 0;
                                                      io.sockets.adapter.rooms[socket.room].user1win = 0;
                                                      io.sockets.adapter.rooms[socket.room].user3win = 0;
                                                      io.sockets.adapter.rooms[socket.room].user4win = 0;
                                                      io.sockets.adapter.rooms[socket.room].boresh = 0;
                                                      io.sockets.adapter.rooms[socket.room].sar = 0;
                                                      io.sockets.adapter.rooms[socket.room].zamin = '';

                                                      username = io.sockets.adapter.rooms[socket.room].users[0];
                                                      socketid = io.sockets.adapter.rooms[socket.room].socks[0];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[0],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[1],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[2],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[3],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[4],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[20],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[21],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[22],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[23],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[36],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[37],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[38],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[39]
                                                      ];
                                                      if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                                                          console.log('send first hand to hakem ', username, io.sockets.adapter.rooms[socket.room].hakem, [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]]);
                                                          io.sockets.connected[socketid].emit("set hand", [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                                                          ]);
                                                      }

                                                      username = io.sockets.adapter.rooms[socket.room].users[1];
                                                      socketid = io.sockets.adapter.rooms[socket.room].socks[1];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[5],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[6],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[7],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[8],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[9],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[24],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[25],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[26],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[27],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[40],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[41],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[42],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[43]
                                                      ];
                                                      if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                                                          console.log('send first hand to hakem ', username, io.sockets.adapter.rooms[socket.room].hakem, [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]]);
                                                          io.sockets.connected[socketid].emit("set hand", [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                                                          ]);
                                                      }

                                                      username = io.sockets.adapter.rooms[socket.room].users[2];
                                                      socketid = io.sockets.adapter.rooms[socket.room].socks[2];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[10],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[11],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[12],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[13],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[14],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[28],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[29],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[30],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[31],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[44],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[45],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[46],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[47]
                                                      ];
                                                      if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                                                          console.log('send first hand to hakem ', username, io.sockets.adapter.rooms[socket.room].hakem, [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]]);
                                                          io.sockets.connected[socketid].emit("set hand", [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                                                          ]);
                                                      }

                                                      username = io.sockets.adapter.rooms[socket.room].users[3];
                                                      socketid = io.sockets.adapter.rooms[socket.room].socks[3];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                                                      io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[15],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[16],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[17],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[18],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[19],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[32],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[33],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[34],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[35],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[48],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[49],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[50],
                                                          io.sockets.adapter.rooms[socket.room].dastRnd[51]
                                                      ];
                                                      if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                                                          console.log('send first hand to hakem ', username, io.sockets.adapter.rooms[socket.room].hakem, [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]]);
                                                          io.sockets.connected[socketid].emit("set hand", [
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                                              io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                                                          ]);
                                                      }

                                                      //io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].winner;
                                                      console.log('team 1 : ' + io.sockets.adapter.rooms[socket.room].user1 + ' & ' + io.sockets.adapter.rooms[socket.room].user3);
                                                      console.log('team 2 : ' + io.sockets.adapter.rooms[socket.room].user2 + ' & ' + io.sockets.adapter.rooms[socket.room].user4);
                                                      io.sockets.adapter.rooms[socket.room].gamewinner = '0';
                                                      io.sockets.adapter.rooms[socket.room].hokm = '';
                                                      io.sockets.adapter.rooms[socket.room].m1 = 0;
                                                      io.sockets.adapter.rooms[socket.room].m2 = 0;
                                                      io.sockets.adapter.rooms[socket.room].m3 = 0;
                                                      io.sockets.adapter.rooms[socket.room].m4 = 0;
                                                      io.sockets.adapter.rooms[socket.room].sh_flag = true;
                                                  }
                                              });
                                          } else if (io.sockets.adapter.rooms[socket.room].gamewinner != '0') {
                                              console.log('game finished - winner : ' + io.sockets.adapter.rooms[socket.room].gamewinner);
                                              if (io.sockets.adapter.rooms[socket.room].gamewinner == io.sockets.adapter.rooms[socket.room].user1 + ',' + io.sockets.adapter.rooms[socket.room].user3) {
                                                  console.log('game winner team 1 : (' + io.sockets.adapter.rooms[socket.room].user1 + ' , ' + io.sockets.adapter.rooms[socket.room].user1 + ')');
                                              } else if (io.sockets.adapter.rooms[socket.room].gamewinner = io.sockets.adapter.rooms[socket.room].user2 + ',' + io.sockets.adapter.rooms[socket.room].user4) {
                                                  console.log('game winner team 2 : (' + io.sockets.adapter.rooms[socket.room].user2 + ' , ' + io.sockets.adapter.rooms[socket.room].user4 + ')');
                                              }
                                              var collection = db.collection('winners');
                                              var time = Math.floor(new Date() / 1000);
                                              var move = {
                                                  g: socket.room,
                                                  s: io.sockets.adapter.rooms[socket.room].step,
                                                  d: data,
                                                  m: io.sockets.adapter.rooms[socket.room].m,
                                                  u1: io.sockets.adapter.rooms[socket.room].user1,
                                                  u1h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user1],
                                                  u2: io.sockets.adapter.rooms[socket.room].user2,
                                                  u2h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user2],
                                                  u3: io.sockets.adapter.rooms[socket.room].user3,
                                                  u3h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user3],
                                                  u4: io.sockets.adapter.rooms[socket.room].user4,
                                                  u4h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user4],
                                                  u: from,
                                                  c: io.sockets.adapter.rooms[socket.room].m,
                                                  u1s: io.sockets.adapter.rooms[socket.room].user1win,
                                                  u2s: io.sockets.adapter.rooms[socket.room].user2win,
                                                  u3s: io.sockets.adapter.rooms[socket.room].user3win,
                                                  u4s: io.sockets.adapter.rooms[socket.room].user4win,
                                                  t1s: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
                                                  t2s: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
                                                  w: io.sockets.adapter.rooms[socket.room].winner,
                                                  sw13: io.sockets.adapter.rooms[socket.room].setwinner13,
                                                  sw24: io.sockets.adapter.rooms[socket.room].setwinner24,
                                                  gw: io.sockets.adapter.rooms[socket.room].gamewinner,
                                                  hokm: io.sockets.adapter.rooms[socket.room].hokm,
                                                  hakem: io.sockets.adapter.rooms[socket.room].hakem,
                                                  n: io.sockets.adapter.rooms[socket.room].nobat,
                                                  t: time
                                              };
                                              collection.insert([move], function (err, result) {
                                                  if (err) {
                                                      console.log(err);
                                                  } else {
                                                      //console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
                                                  }
                                              });
                                              io.sockets.in(socket.room).emit('game finish', {
                                                  user1: io.sockets.adapter.rooms[socket.room].user1,
                                                  user2: io.sockets.adapter.rooms[socket.room].user2,
                                                  user3: io.sockets.adapter.rooms[socket.room].user3,
                                                  user4: io.sockets.adapter.rooms[socket.room].user4,
                                                  user1score: io.sockets.adapter.rooms[socket.room].user1win,
                                                  user2score: io.sockets.adapter.rooms[socket.room].user2win,
                                                  user3score: io.sockets.adapter.rooms[socket.room].user3win,
                                                  user4score: io.sockets.adapter.rooms[socket.room].user4win,
                                                  gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
                                                  setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
                                                  setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24
                                              });
                                              io.sockets.adapter.rooms[socket.room].ips = [];
                                              io.sockets.adapter.rooms[socket.room].users = [];
                                              io.sockets.adapter.rooms[socket.room].tableUsers = [];
                                              io.sockets.adapter.rooms[socket.room].socks = [];
                                              io.sockets.adapter.rooms[socket.room].tableSocks = [];
                                              io.sockets.adapter.rooms[socket.room].user1 = '';
                                              io.sockets.adapter.rooms[socket.room].user2 = '';
                                              io.sockets.adapter.rooms[socket.room].user3 = '';
                                              io.sockets.adapter.rooms[socket.room].user4 = '';
                                              io.sockets.adapter.rooms[socket.room].hokm = '';
                                              io.sockets.adapter.rooms[socket.room].hakem = '';
                                              io.sockets.adapter.rooms[socket.room].yarHakem = '';
                                              io.sockets.adapter.rooms[socket.room].hakem_code = 0;
                                              io.sockets.adapter.rooms[socket.room].yarHakem_code = 0;
                                              io.sockets.adapter.rooms[socket.room].nobat = '';
                                              io.sockets.adapter.rooms[socket.room].zamin = '';
                                              io.sockets.adapter.rooms[socket.room].dastRnd = [];
                                              io.sockets.adapter.rooms[socket.room].taeenHakemRnd = [];
                                              io.sockets.adapter.rooms[socket.room].usersHands = [];
                                              io.sockets.adapter.rooms[socket.room].handflag = false;
                                              io.sockets.adapter.rooms[socket.room].boresh = 0;
                                              io.sockets.adapter.rooms[socket.room].sar = 0;
                                              io.sockets.adapter.rooms[socket.room].kosbazi = 0;
                                              io.sockets.adapter.rooms[socket.room].user1win = 0;
                                              io.sockets.adapter.rooms[socket.room].user2win = 0;
                                              io.sockets.adapter.rooms[socket.room].user3win = 0;
                                              io.sockets.adapter.rooms[socket.room].user4win = 0;
                                              io.sockets.adapter.rooms[socket.room].m1 = 0;
                                              io.sockets.adapter.rooms[socket.room].m2 = 0;
                                              io.sockets.adapter.rooms[socket.room].m3 = 0;
                                              io.sockets.adapter.rooms[socket.room].m4 = 0;
                                              io.sockets.adapter.rooms[socket.room].winner = 0;
                                              io.sockets.adapter.rooms[socket.room].min = 0;
                                              io.sockets.adapter.rooms[socket.room].max = 0;
                                              io.sockets.adapter.rooms[socket.room].minhokm = 0;
                                              io.sockets.adapter.rooms[socket.room].maxhokm = 0;
                                              io.sockets.adapter.rooms[socket.room].minzamin = 0;
                                              io.sockets.adapter.rooms[socket.room].maxzamin = 0;
                                              io.sockets.adapter.rooms[socket.room].setwinner13 = 0;
                                              io.sockets.adapter.rooms[socket.room].setwinner24 = 0;
                                              io.sockets.adapter.rooms[socket.room].gamewinner13 = 0;
                                              io.sockets.adapter.rooms[socket.room].gamewinner24 = 0;
                                              io.sockets.adapter.rooms[socket.room].gamewinner = '';
                                              io.sockets.adapter.rooms[socket.room].del_dare = true;
                                              io.sockets.adapter.rooms[socket.room].pik_dare = true;
                                              io.sockets.adapter.rooms[socket.room].khesht_dare = true;
                                              io.sockets.adapter.rooms[socket.room].sh_flag = false;
                                              io.sockets.adapter.rooms[socket.room].gish_dare = true;
                                          }
                                          io.sockets.adapter.rooms[socket.room].boresh = 0;
                                          io.sockets.adapter.rooms[socket.room].sar = 0;
                                          io.sockets.adapter.rooms[socket.room].m1 = 0;
                                          io.sockets.adapter.rooms[socket.room].m2 = 0;
                                          io.sockets.adapter.rooms[socket.room].m3 = 0;
                                          io.sockets.adapter.rooms[socket.room].m4 = 0;
                                          io.sockets.adapter.rooms[socket.room].zamin = '';
                                      }
                                      console.log('nobat : ' + io.sockets.adapter.rooms[socket.room].nobat);
                                      /*
                                       if (io.sockets.adapter.rooms[socket.room].handflag) {
                                       io.sockets.adapter.rooms[socket.room].handflag = false;
                                       io.sockets.in(socket.room).emit('new hand', {
                                       username: from,
                                       card: io.sockets.adapter.rooms[socket.room].m,
                                       user1: io.sockets.adapter.rooms[socket.room].user1,
                                       user2: io.sockets.adapter.rooms[socket.room].user2,
                                       user3: io.sockets.adapter.rooms[socket.room].user3,
                                       user4: io.sockets.adapter.rooms[socket.room].user4,
                                       user1score: io.sockets.adapter.rooms[socket.room].user1win,
                                       user2score: io.sockets.adapter.rooms[socket.room].user2win,
                                       user3score: io.sockets.adapter.rooms[socket.room].user3win,
                                       user4score: io.sockets.adapter.rooms[socket.room].user4win,
                                       team1score: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
                                       team2score: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
                                       winner: io.sockets.adapter.rooms[socket.room].winner,
                                       setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
                                       setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24,
                                       gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
                                       hokm: io.sockets.adapter.rooms[socket.room].hokm,
                                       hakem: io.sockets.adapter.rooms[socket.room].hakem,
                                       nobat: io.sockets.adapter.rooms[socket.room].nobat
                                       });
                                       } else {
                                       */
                                      io.sockets.adapter.rooms[socket.room].step++;
                                      var collection = db.collection('moves');
                                      var time = Math.floor(new Date() / 1000);
                                      var move = {
                                          g: socket.room,
                                          s: io.sockets.adapter.rooms[socket.room].step,
                                          d: data,
                                          m: io.sockets.adapter.rooms[socket.room].m,
                                          u1: io.sockets.adapter.rooms[socket.room].user1,
                                          u1h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user1],
                                          u2: io.sockets.adapter.rooms[socket.room].user2,
                                          u2h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user2],
                                          u3: io.sockets.adapter.rooms[socket.room].user3,
                                          u3h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user3],
                                          u4: io.sockets.adapter.rooms[socket.room].user4,
                                          u4h: io.sockets.adapter.rooms[socket.room].usersHands[io.sockets.adapter.rooms[socket.room].user4],
                                          m1: io.sockets.adapter.rooms[socket.room].m1,
                                          m2: io.sockets.adapter.rooms[socket.room].m2,
                                          m3: io.sockets.adapter.rooms[socket.room].m3,
                                          m4: io.sockets.adapter.rooms[socket.room].m4,
                                          u: from,
                                          c: io.sockets.adapter.rooms[socket.room].m,
                                          u1s: io.sockets.adapter.rooms[socket.room].user1win,
                                          u2s: io.sockets.adapter.rooms[socket.room].user2win,
                                          u3s: io.sockets.adapter.rooms[socket.room].user3win,
                                          u4s: io.sockets.adapter.rooms[socket.room].user4win,
                                          t1s: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
                                          t2s: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
                                          w: io.sockets.adapter.rooms[socket.room].winner,
                                          sw13: io.sockets.adapter.rooms[socket.room].setwinner13,
                                          sw24: io.sockets.adapter.rooms[socket.room].setwinner24,
                                          gw: io.sockets.adapter.rooms[socket.room].gamewinner,
                                          hokm: io.sockets.adapter.rooms[socket.room].hokm,
                                          hakem: io.sockets.adapter.rooms[socket.room].hakem,
                                          n: io.sockets.adapter.rooms[socket.room].nobat,
                                          t: time
                                      };
                                      if (io.sockets.adapter.rooms[socket.room].step == 4)
                                          io.sockets.adapter.rooms[socket.room].step = 0;
                                      collection.insert([move], function (err, result) {
                                          if (err) {
                                              console.log(err);
                                          } else {
                                              //console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
                                          }
                                      });
                                      io.sockets.in(socket.room).emit('new move', {
                                          sec: io.sockets.adapter.rooms[socket.room].time_limit,
                                          username: from,
                                          card: io.sockets.adapter.rooms[socket.room].m,
                                          step: io.sockets.adapter.rooms[socket.room].step,
                                          user1: io.sockets.adapter.rooms[socket.room].user1,
                                          user2: io.sockets.adapter.rooms[socket.room].user2,
                                          user3: io.sockets.adapter.rooms[socket.room].user3,
                                          user4: io.sockets.adapter.rooms[socket.room].user4,
                                          m1: io.sockets.adapter.rooms[socket.room].m1,
                                          m2: io.sockets.adapter.rooms[socket.room].m2,
                                          m3: io.sockets.adapter.rooms[socket.room].m3,
                                          m4: io.sockets.adapter.rooms[socket.room].m4,
                                          zamin: io.sockets.adapter.rooms[socket.room].zamin,
                                          minzamin: io.sockets.adapter.rooms[socket.room].minzamin,
                                          maxzamin: io.sockets.adapter.rooms[socket.room].maxzamin,
                                          user1score: io.sockets.adapter.rooms[socket.room].user1win,
                                          user2score: io.sockets.adapter.rooms[socket.room].user2win,
                                          user3score: io.sockets.adapter.rooms[socket.room].user3win,
                                          user4score: io.sockets.adapter.rooms[socket.room].user4win,
                                          team1score: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
                                          team2score: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
                                          winner: io.sockets.adapter.rooms[socket.room].winner,
                                          setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
                                          setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24,
                                          gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
                                          hokm: io.sockets.adapter.rooms[socket.room].hokm,
                                          minhokm: io.sockets.adapter.rooms[socket.room].minhokm,
                                          maxhokm: io.sockets.adapter.rooms[socket.room].maxhokm,
                                          hakem: io.sockets.adapter.rooms[socket.room].hakem,
                                          nobat: io.sockets.adapter.rooms[socket.room].nobat
                                      });
                                      //}
                                  }
                              }
                          } else {
                              console.log('nobate shoma nist : ' + from);
                              io.sockets.connected[socket.id].emit('new message', {
                                  username: from,
                                  message: 'نوبت شما نیست!'
                              });
                          }
                      } else {
                          console.log('hokm hanoz taeen nashode : ' + from);
                          io.sockets.connected[socket.id].emit('new message', {
                              username: from,
                              message: 'حکم هنوز تعیین نشده!'
                          });
                      }
                  } else if (data.substring(0, 2) == 'h:') {
                      io.sockets.adapter.rooms[socket.room].m = 0;
                      console.log(from + ' send selected hokm : ' + data);
                      if (io.sockets.adapter.rooms[socket.room].hokm == '') {
                          if (io.sockets.adapter.rooms[socket.room].hakem == from) {
                              switch (data) {
                                  case 'h:h':
                                      io.sockets.adapter.rooms[socket.room].hokm = 'hearts';//hokm del
                                      break;
                                  case 'h:s':
                                      io.sockets.adapter.rooms[socket.room].hokm = 'spades';//hokm pik
                                      break;
                                  case 'h:d':
                                      io.sockets.adapter.rooms[socket.room].hokm = 'diamonds';//hokm khesht
                                      break;
                                  case 'h:c':
                                      io.sockets.adapter.rooms[socket.room].hokm = 'clubs';//hokm gishniz
                                      break;
                              }

                              if (io.sockets.adapter.rooms[socket.room].hokm != '') {
                                  username = io.sockets.adapter.rooms[socket.room].users[0];
                                  socketid = io.sockets.adapter.rooms[socket.room].socks[0];

                                  console.log('send all cards to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                                  io.sockets.connected[socketid].emit("set hand",
                                      io.sockets.adapter.rooms[socket.room].usersHands[username]
                                  );

                                  username = io.sockets.adapter.rooms[socket.room].users[1];
                                  socketid = io.sockets.adapter.rooms[socket.room].socks[1];
                                  console.log('send all cards to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                                  io.sockets.connected[socketid].emit("set hand",
                                      io.sockets.adapter.rooms[socket.room].usersHands[username]
                                  );

                                  username = io.sockets.adapter.rooms[socket.room].users[2];
                                  socketid = io.sockets.adapter.rooms[socket.room].socks[2];
                                  console.log('send all cards to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                                  io.sockets.connected[socketid].emit("set hand",
                                      io.sockets.adapter.rooms[socket.room].usersHands[username]
                                  );

                                  username = io.sockets.adapter.rooms[socket.room].users[3];
                                  socketid = io.sockets.adapter.rooms[socket.room].socks[3];
                                  console.log('send all cards to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                                  io.sockets.connected[socketid].emit("set hand",
                                      io.sockets.adapter.rooms[socket.room].usersHands[username]
                                  );

                                  io.sockets.in(socket.room).emit('new move', {
                                      sec: io.sockets.adapter.rooms[socket.room].time_limit,
                                      username: from,
                                      card: io.sockets.adapter.rooms[socket.room].m,
                                      step: io.sockets.adapter.rooms[socket.room].step,
                                      user1: io.sockets.adapter.rooms[socket.room].user1,
                                      user2: io.sockets.adapter.rooms[socket.room].user2,
                                      user3: io.sockets.adapter.rooms[socket.room].user3,
                                      user4: io.sockets.adapter.rooms[socket.room].user4,
                                      m1: io.sockets.adapter.rooms[socket.room].m1,
                                      m2: io.sockets.adapter.rooms[socket.room].m2,
                                      m3: io.sockets.adapter.rooms[socket.room].m3,
                                      m4: io.sockets.adapter.rooms[socket.room].m4,
                                      zamin: io.sockets.adapter.rooms[socket.room].zamin,
                                      minzamin: io.sockets.adapter.rooms[socket.room].minzamin,
                                      maxzamin: io.sockets.adapter.rooms[socket.room].maxzamin,
                                      user1score: io.sockets.adapter.rooms[socket.room].user1win,
                                      user2score: io.sockets.adapter.rooms[socket.room].user2win,
                                      user3score: io.sockets.adapter.rooms[socket.room].user3win,
                                      user4score: io.sockets.adapter.rooms[socket.room].user4win,
                                      team1score: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
                                      team2score: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
                                      winner: io.sockets.adapter.rooms[socket.room].winner,
                                      setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
                                      setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24,
                                      gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
                                      hokm: io.sockets.adapter.rooms[socket.room].hokm,
                                      minhokm: io.sockets.adapter.rooms[socket.room].minhokm,
                                      maxhokm: io.sockets.adapter.rooms[socket.room].maxhokm,
                                      hakem: io.sockets.adapter.rooms[socket.room].hakem,
                                      nobat: io.sockets.adapter.rooms[socket.room].nobat
                                  });
                              } else {
                                  console.log(from + ' cherto pert ferestade be jaye hokm');
                              }
                          } else {
                              console.log('shoma hakem nistid : ' + from);
                              io.sockets.connected[socket.id].emit('new message', {
                                  username: from,
                                  message: 'شما حاکم نیستی که!'
                              });
                          }
                      } else {
                          console.log('hokm ghablan taeen shode : ' + io.sockets.adapter.rooms[socket.room].hokm + ' - hakem : ' + io.sockets.adapter.rooms[socket.room].hakem);
                          io.sockets.connected[socket.id].emit('new message', {
                              username: from,
                              message: 'حکم قبلا تعیین شده!'
                          });
                      }
                  } else {
                      io.sockets.connected[socket.id].emit('new message', {
                          username: from,
                          message: data
                      });
                  }
                  if (io.sockets.adapter.rooms[socket.room].sh_flag == true) {
                      io.sockets.adapter.rooms[socket.room].sh_flag = false;
                      io.sockets.in(socket.room).emit('select hakem', {
                          sec: io.sockets.adapter.rooms[socket.room].time_limit,
                          nobat: io.sockets.adapter.rooms[socket.room].nobat,
                          user1: io.sockets.adapter.rooms[socket.room].user1,
                          user2: io.sockets.adapter.rooms[socket.room].user2,
                          user3: io.sockets.adapter.rooms[socket.room].user3,
                          user4: io.sockets.adapter.rooms[socket.room].user4,
                          taeenHakem: io.sockets.adapter.rooms[socket.room].taeenHakemRnd,
                          hakem: io.sockets.adapter.rooms[socket.room].hakem,
                          yarHakem: io.sockets.adapter.rooms[socket.room].yarHakem
                      });
                  }
              }
          }
      } else {
          console.log('socket in null : ' + socket);
      }
  });

  // vaghti user roye miz mishine
  socket.on('user seat', function (username, chair) {
      var from = socket.username;
      if(io.sockets.adapter.rooms[socket.room].tableUsers) {
          if (io.sockets.adapter.rooms[socket.room].tableUsers.length < 4) {
              //update number of table users

              // we store the username in the socket session for this client
              io.sockets.adapter.rooms[socket.room].tableUsers.push(username);
              io.sockets.adapter.rooms[socket.room].tableSocks.push(socket.id);

              io.sockets.in(socket.room).emit('login', {
                  numUsers: io.sockets.adapter.rooms[socket.room].users.length,
                  tableNumUsers: io.sockets.adapter.rooms[socket.room].tableUsers.length
              });

              console.log('user : ' + from + ' seat on chair : ' + chair);
              // echo globally (all clients) that a person has connected

              io.sockets.in(socket.room).emit('user seat', {
                  user1: io.sockets.adapter.rooms[socket.room].user1,
                  user2: io.sockets.adapter.rooms[socket.room].user2,
                  user3: io.sockets.adapter.rooms[socket.room].user3,
                  user4: io.sockets.adapter.rooms[socket.room].user4,
                  taeenHakem: io.sockets.adapter.rooms[socket.room].taeenHakemRnd,
                  hakem: io.sockets.adapter.rooms[socket.room].hakem,
                  yarHakem: io.sockets.adapter.rooms[socket.room].yarHakem,
                  username: from,
                  chair: chair,
                  numUsers: io.sockets.adapter.rooms[socket.room].users.length,
                  tableNumUsers: io.sockets.adapter.rooms[socket.room].tableUsers.length
              });
              connection.query('UPDATE hokm_rooms SET online_no = online_no + 1 WHERE id=' + r, function (err, roomr, fields) {
                  if (err) {
                      console.log(err);
                  }
              })

              if (io.sockets.adapter.rooms[socket.room].tableUsers.length == 4) {
                  setHands(socket.room, function (err, data) {
                      if (err) {
                          // error handling code goes here
                          console.log("ERROR : ", err);
                      } else {
                          io.sockets.adapter.rooms[socket.room].nobat = io.sockets.adapter.rooms[socket.room].hakem;
                          io.sockets.adapter.rooms[socket.room].step = 0;
                          io.sockets.adapter.rooms[socket.room].m1 = 0;
                          io.sockets.adapter.rooms[socket.room].m2 = 0;
                          io.sockets.adapter.rooms[socket.room].m3 = 0;
                          io.sockets.adapter.rooms[socket.room].m4 = 0;
                          io.sockets.adapter.rooms[socket.room].user1win = 0;
                          io.sockets.adapter.rooms[socket.room].user2win = 0;
                          io.sockets.adapter.rooms[socket.room].user3win = 0;
                          io.sockets.adapter.rooms[socket.room].user4win = 0;
                          io.sockets.adapter.rooms[socket.room].boresh = 0;
                          io.sockets.adapter.rooms[socket.room].sar = 0;
                          io.sockets.adapter.rooms[socket.room].zamin = '';

                          username = io.sockets.adapter.rooms[socket.room].users[0];
                          socketid = io.sockets.adapter.rooms[socket.room].socks[0];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                              io.sockets.adapter.rooms[socket.room].dastRnd[0],
                              io.sockets.adapter.rooms[socket.room].dastRnd[1],
                              io.sockets.adapter.rooms[socket.room].dastRnd[2],
                              io.sockets.adapter.rooms[socket.room].dastRnd[3],
                              io.sockets.adapter.rooms[socket.room].dastRnd[4],
                              io.sockets.adapter.rooms[socket.room].dastRnd[20],
                              io.sockets.adapter.rooms[socket.room].dastRnd[21],
                              io.sockets.adapter.rooms[socket.room].dastRnd[22],
                              io.sockets.adapter.rooms[socket.room].dastRnd[23],
                              io.sockets.adapter.rooms[socket.room].dastRnd[36],
                              io.sockets.adapter.rooms[socket.room].dastRnd[37],
                              io.sockets.adapter.rooms[socket.room].dastRnd[38],
                              io.sockets.adapter.rooms[socket.room].dastRnd[39]
                          ];
                          if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                              console.log('in start game send first hand to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                              io.sockets.connected[socketid].emit("set hand", [
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                              ]);
                          }

                          username = io.sockets.adapter.rooms[socket.room].users[1];
                          socketid = io.sockets.adapter.rooms[socket.room].socks[1];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                              io.sockets.adapter.rooms[socket.room].dastRnd[5],
                              io.sockets.adapter.rooms[socket.room].dastRnd[6],
                              io.sockets.adapter.rooms[socket.room].dastRnd[7],
                              io.sockets.adapter.rooms[socket.room].dastRnd[8],
                              io.sockets.adapter.rooms[socket.room].dastRnd[9],
                              io.sockets.adapter.rooms[socket.room].dastRnd[24],
                              io.sockets.adapter.rooms[socket.room].dastRnd[25],
                              io.sockets.adapter.rooms[socket.room].dastRnd[26],
                              io.sockets.adapter.rooms[socket.room].dastRnd[27],
                              io.sockets.adapter.rooms[socket.room].dastRnd[40],
                              io.sockets.adapter.rooms[socket.room].dastRnd[41],
                              io.sockets.adapter.rooms[socket.room].dastRnd[42],
                              io.sockets.adapter.rooms[socket.room].dastRnd[43]
                          ];
                          if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                              console.log('in start game send first hand to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                              io.sockets.connected[socketid].emit("set hand", [
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                              ]);
                          }

                          username = io.sockets.adapter.rooms[socket.room].users[2];
                          socketid = io.sockets.adapter.rooms[socket.room].socks[2];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                              io.sockets.adapter.rooms[socket.room].dastRnd[10],
                              io.sockets.adapter.rooms[socket.room].dastRnd[11],
                              io.sockets.adapter.rooms[socket.room].dastRnd[12],
                              io.sockets.adapter.rooms[socket.room].dastRnd[13],
                              io.sockets.adapter.rooms[socket.room].dastRnd[14],
                              io.sockets.adapter.rooms[socket.room].dastRnd[28],
                              io.sockets.adapter.rooms[socket.room].dastRnd[29],
                              io.sockets.adapter.rooms[socket.room].dastRnd[30],
                              io.sockets.adapter.rooms[socket.room].dastRnd[31],
                              io.sockets.adapter.rooms[socket.room].dastRnd[44],
                              io.sockets.adapter.rooms[socket.room].dastRnd[45],
                              io.sockets.adapter.rooms[socket.room].dastRnd[46],
                              io.sockets.adapter.rooms[socket.room].dastRnd[47]
                          ];
                          if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                              console.log('in start game send first hand to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                              io.sockets.connected[socketid].emit('set hand', [
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                              ]);
                          }

                          username = io.sockets.adapter.rooms[socket.room].users[3];
                          socketid = io.sockets.adapter.rooms[socket.room].socks[3];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [];
                          io.sockets.adapter.rooms[socket.room].usersHands[username] = [
                              io.sockets.adapter.rooms[socket.room].dastRnd[15],
                              io.sockets.adapter.rooms[socket.room].dastRnd[16],
                              io.sockets.adapter.rooms[socket.room].dastRnd[17],
                              io.sockets.adapter.rooms[socket.room].dastRnd[18],
                              io.sockets.adapter.rooms[socket.room].dastRnd[19],
                              io.sockets.adapter.rooms[socket.room].dastRnd[32],
                              io.sockets.adapter.rooms[socket.room].dastRnd[33],
                              io.sockets.adapter.rooms[socket.room].dastRnd[34],
                              io.sockets.adapter.rooms[socket.room].dastRnd[35],
                              io.sockets.adapter.rooms[socket.room].dastRnd[48],
                              io.sockets.adapter.rooms[socket.room].dastRnd[49],
                              io.sockets.adapter.rooms[socket.room].dastRnd[50],
                              io.sockets.adapter.rooms[socket.room].dastRnd[51]
                          ];
                          if (username == io.sockets.adapter.rooms[socket.room].hakem) {
                              console.log('in start game send first hand to ', username, io.sockets.adapter.rooms[socket.room].hakem);
                              io.sockets.connected[socketid].emit('set hand', [
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][0],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][1],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][2],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][3],
                                  io.sockets.adapter.rooms[socket.room].usersHands[username][4]
                              ]);
                          }
                          io.sockets.adapter.rooms[socket.room].gamewinner = '0';
                          io.sockets.adapter.rooms[socket.room].hokm = '';
                          io.sockets.in(socket.room).emit('select hakem', {
                              sec: io.sockets.adapter.rooms[socket.room].time_limit,
                              nobat: io.sockets.adapter.rooms[socket.room].nobat,
                              user1: io.sockets.adapter.rooms[socket.room].user1,
                              user2: io.sockets.adapter.rooms[socket.room].user2,
                              user3: io.sockets.adapter.rooms[socket.room].user3,
                              user4: io.sockets.adapter.rooms[socket.room].user4,
                              taeenHakem: io.sockets.adapter.rooms[socket.room].taeenHakemRnd,
                              hakem: io.sockets.adapter.rooms[socket.room].hakem,
                              yarHakem: io.sockets.adapter.rooms[socket.room].yarHakem
                          });
                      }
                  });
              }
          } else {
              console.log('hame sandaliha por ast ' + username);
          }
      }
  });

  // vaghti user varede room mishe
  socket.on('user login room', function (userid, username, room, game_limit, time_limit) {
    var ip = socket.request.connection.remoteAddress;
    ip = ip.replace('::ffff:', '');
    var collection = db.collection('users');
    var time = Math.floor(new Date() / 1000);
    var user1 = {ui: userid, u: username, r: room, gl: game_limit, ip:ip, t:time};
      collection.insert([user1], function (err, result) {
          if (err) {
              console.log(err);
          } else {
              //console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
          }
      });
    // store the username in the socket session for this client
    socket.username = username;
    // store the room name in the socket session for this client
    socket.room = room;
    // add the client's username to the global list
    socket.join(room);
    socket.username = username;
      var from = username;
    if(io.sockets.adapter.rooms[socket.room].users == undefined){
      io.sockets.adapter.rooms[socket.room].ips = [];
      io.sockets.adapter.rooms[socket.room].users = [];
      io.sockets.adapter.rooms[socket.room].disconnecteds = [];
      io.sockets.adapter.rooms[socket.room].tableUsers = [];
      io.sockets.adapter.rooms[socket.room].socks = [];
      io.sockets.adapter.rooms[socket.room].tableSocks = [];
      io.sockets.adapter.rooms[socket.room].user1 = '';
      io.sockets.adapter.rooms[socket.room].user2 = '';
      io.sockets.adapter.rooms[socket.room].user3 = '';
      io.sockets.adapter.rooms[socket.room].user4 = '';
      io.sockets.adapter.rooms[socket.room].hokm = '';
      io.sockets.adapter.rooms[socket.room].hakem = '';
      io.sockets.adapter.rooms[socket.room].yarHakem = '';
      io.sockets.adapter.rooms[socket.room].hakem_code = 0;
      io.sockets.adapter.rooms[socket.room].yarHakem_code = 0;
      io.sockets.adapter.rooms[socket.room].nobat = '';
      io.sockets.adapter.rooms[socket.room].zamin = '';
      io.sockets.adapter.rooms[socket.room].dastRnd = [];
      io.sockets.adapter.rooms[socket.room].taeenHakemRnd = [];
      io.sockets.adapter.rooms[socket.room].usersHands = [];
      io.sockets.adapter.rooms[socket.room].game_limit = game_limit;
      io.sockets.adapter.rooms[socket.room].time_limit = time_limit;
      io.sockets.adapter.rooms[socket.room].handflag = false;
      io.sockets.adapter.rooms[socket.room].boresh = 0;
      io.sockets.adapter.rooms[socket.room].sar = 0;
      io.sockets.adapter.rooms[socket.room].kosbazi = 0;
      io.sockets.adapter.rooms[socket.room].user1win = 0;
      io.sockets.adapter.rooms[socket.room].user2win = 0;
      io.sockets.adapter.rooms[socket.room].user3win = 0;
      io.sockets.adapter.rooms[socket.room].user4win = 0;
      io.sockets.adapter.rooms[socket.room].step = 0;
      io.sockets.adapter.rooms[socket.room].m1 = 0;
      io.sockets.adapter.rooms[socket.room].m2 = 0;
      io.sockets.adapter.rooms[socket.room].m3 = 0;
      io.sockets.adapter.rooms[socket.room].m4 = 0;
      io.sockets.adapter.rooms[socket.room].winner = 0;
      io.sockets.adapter.rooms[socket.room].min = 0;
      io.sockets.adapter.rooms[socket.room].max = 0;
      io.sockets.adapter.rooms[socket.room].minhokm = 0;
      io.sockets.adapter.rooms[socket.room].maxhokm = 0;
      io.sockets.adapter.rooms[socket.room].minzamin = 0;
      io.sockets.adapter.rooms[socket.room].maxzamin = 0;
      io.sockets.adapter.rooms[socket.room].setwinner13 = 0;
      io.sockets.adapter.rooms[socket.room].setwinner24 = 0;
      io.sockets.adapter.rooms[socket.room].gamewinner13 = 0;
      io.sockets.adapter.rooms[socket.room].gamewinner24 = 0;
      io.sockets.adapter.rooms[socket.room].gamewinner = '';
      io.sockets.adapter.rooms[socket.room].del_dare = true;
      io.sockets.adapter.rooms[socket.room].pik_dare = true;
      io.sockets.adapter.rooms[socket.room].khesht_dare = true;
      io.sockets.adapter.rooms[socket.room].sh_flag = false;
      io.sockets.adapter.rooms[socket.room].gish_dare = true;
    }
      console.log(username + ' return ' + socket.id);
      var index = io.sockets.adapter.rooms[room].users.indexOf(username);
      if (index > -1) {
          io.sockets.adapter.rooms[socket.room].ips.push(ip);
          io.sockets.adapter.rooms[socket.room].socks[index] = socket.id;
          io.sockets.adapter.rooms[socket.room].tableSocks[index] = socket.id;
      }
    if (io.sockets.adapter.rooms[room].tableUsers.indexOf(username) != -1) {
        io.sockets.connected[socket.id].emit('select hakem', {
            sec: io.sockets.adapter.rooms[socket.room].time_limit,
            nobat: io.sockets.adapter.rooms[socket.room].nobat,
            user1: io.sockets.adapter.rooms[socket.room].user1,
            user2: io.sockets.adapter.rooms[socket.room].user2,
            user3: io.sockets.adapter.rooms[socket.room].user3,
            user4: io.sockets.adapter.rooms[socket.room].user4,
            taeenHakem: io.sockets.adapter.rooms[socket.room].taeenHakemRnd,
            hakem: io.sockets.adapter.rooms[socket.room].hakem,
            yarHakem: io.sockets.adapter.rooms[socket.room].yarHakem
        });
        io.sockets.connected[socket.id].emit('user seat', {
            user1: io.sockets.adapter.rooms[socket.room].user1,
            user2: io.sockets.adapter.rooms[socket.room].user2,
            user3: io.sockets.adapter.rooms[socket.room].user3,
            user4: io.sockets.adapter.rooms[socket.room].user4,
            taeenHakem: io.sockets.adapter.rooms[socket.room].taeenHakemRnd,
            hakem: io.sockets.adapter.rooms[socket.room].hakem,
            yarHakem: io.sockets.adapter.rooms[socket.room].yarHakem,
            username: from,
            chair: '4',
            numUsers: io.sockets.adapter.rooms[socket.room].users.length,
            tableNumUsers: io.sockets.adapter.rooms[socket.room].tableUsers.length
        });
      if (io.sockets.adapter.rooms[socket.room].hakem == username &&
          io.sockets.adapter.rooms[socket.room].hokm == '' &&
          io.sockets.adapter.rooms[socket.room].nobat == username &&
          io.sockets.adapter.rooms[socket.room].m1 == 0 &&
          io.sockets.adapter.rooms[socket.room].m2 == 0 &&
          io.sockets.adapter.rooms[socket.room].m3 == 0 &&
          io.sockets.adapter.rooms[socket.room].m4 == 0
      ) {
          io.sockets.connected[socket.id].emit("set hand",[
              io.sockets.adapter.rooms[socket.room].usersHands[username][0],
              io.sockets.adapter.rooms[socket.room].usersHands[username][1],
              io.sockets.adapter.rooms[socket.room].usersHands[username][2],
              io.sockets.adapter.rooms[socket.room].usersHands[username][3],
              io.sockets.adapter.rooms[socket.room].usersHands[username][4]
          ]);
          io.sockets.connected[socket.id].emit('select hakem', {
              sec: io.sockets.adapter.rooms[socket.room].time_limit,
              nobat: io.sockets.adapter.rooms[socket.room].nobat,
              user1: io.sockets.adapter.rooms[socket.room].user1,
              user2: io.sockets.adapter.rooms[socket.room].user2,
              user3: io.sockets.adapter.rooms[socket.room].user3,
              user4: io.sockets.adapter.rooms[socket.room].user4,
              taeenHakem: io.sockets.adapter.rooms[socket.room].taeenHakemRnd,
              hakem: io.sockets.adapter.rooms[socket.room].hakem,
              yarHakem: io.sockets.adapter.rooms[socket.room].yarHakem
          });
      } else {
          if(io.sockets.adapter.rooms[socket.room].hokm != '') {
              io.sockets.connected[socket.id].emit("set hand",
                  io.sockets.adapter.rooms[socket.room].usersHands[username]
              );
              io.sockets.connected[socket.id].emit("current hands", {
                  username: from,
                  card: io.sockets.adapter.rooms[socket.room].m,
                  user1: io.sockets.adapter.rooms[socket.room].user1,
                  user2: io.sockets.adapter.rooms[socket.room].user2,
                  user3: io.sockets.adapter.rooms[socket.room].user3,
                  user4: io.sockets.adapter.rooms[socket.room].user4,
                  user1score: io.sockets.adapter.rooms[socket.room].user1win,
                  user2score: io.sockets.adapter.rooms[socket.room].user2win,
                  user3score: io.sockets.adapter.rooms[socket.room].user3win,
                  user4score: io.sockets.adapter.rooms[socket.room].user4win,
                  team1score: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
                  team2score: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
                  winner: io.sockets.adapter.rooms[socket.room].winner,
                  setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
                  setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24,
                  gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
                  hokm: io.sockets.adapter.rooms[socket.room].hokm,
                  hakem: io.sockets.adapter.rooms[socket.room].hakem,
                  nobat: io.sockets.adapter.rooms[socket.room].nobat,
                  m1: io.sockets.adapter.rooms[socket.room].m1,
                  m2: io.sockets.adapter.rooms[socket.room].m2,
                  m3: io.sockets.adapter.rooms[socket.room].m3,
                  m4: io.sockets.adapter.rooms[socket.room].m4
              });
          }
      }
      var index = io.sockets.adapter.rooms[socket.room].disconnecteds.indexOf(username);
      if (index > -1) {
        io.sockets.adapter.rooms[socket.room].disconnecteds.splice(index, 1);
      }
      console.log('user : ' + username + ' returned to room : ' + socket.id);
    } else {
        var dev =true;
        if (io.sockets.adapter.rooms[room].ips.indexOf(ip) == -1 || dev) {
            io.sockets.adapter.rooms[socket.room].ips.push(ip);
            io.sockets.adapter.rooms[socket.room].users.push(username);
            io.sockets.adapter.rooms[socket.room].socks.push(socket.id);

            io.sockets.in(socket.room).emit('update chat', io.sockets.adapter.rooms[room].users, username + ' وارد اتاق شد');
            console.log('user : ' + username + ' log in room : ' + room);
        } else {
            console.log('permission denied ' + userid + ',' + username + ',' + room + ',' + game_limit);
        }
    }
      if(io.sockets.adapter.rooms[socket.room].tableUsers.length >= 4 &&
          io.sockets.adapter.rooms[socket.room].user1 != '' &&
          io.sockets.adapter.rooms[socket.room].user2 != '' &&
          io.sockets.adapter.rooms[socket.room].user3 != '' &&
          io.sockets.adapter.rooms[socket.room].user4 != ''
      ){
          io.sockets.connected[socket.id].emit("guest view", {
              username: from,
              card: io.sockets.adapter.rooms[socket.room].m,
              user1: io.sockets.adapter.rooms[socket.room].user1,
              user2: io.sockets.adapter.rooms[socket.room].user2,
              user3: io.sockets.adapter.rooms[socket.room].user3,
              user4: io.sockets.adapter.rooms[socket.room].user4,
              user1score: io.sockets.adapter.rooms[socket.room].user1win,
              user2score: io.sockets.adapter.rooms[socket.room].user2win,
              user3score: io.sockets.adapter.rooms[socket.room].user3win,
              user4score: io.sockets.adapter.rooms[socket.room].user4win,
              team1score: io.sockets.adapter.rooms[socket.room].user1win + io.sockets.adapter.rooms[socket.room].user3win,
              team2score: io.sockets.adapter.rooms[socket.room].user2win + io.sockets.adapter.rooms[socket.room].user4win,
              winner: io.sockets.adapter.rooms[socket.room].winner,
              setwinner13: io.sockets.adapter.rooms[socket.room].setwinner13,
              setwinner24: io.sockets.adapter.rooms[socket.room].setwinner24,
              gamewinner: io.sockets.adapter.rooms[socket.room].gamewinner,
              hokm: io.sockets.adapter.rooms[socket.room].hokm,
              hakem: io.sockets.adapter.rooms[socket.room].hakem,
              nobat: io.sockets.adapter.rooms[socket.room].nobat,
              m1: io.sockets.adapter.rooms[socket.room].m1,
              m2: io.sockets.adapter.rooms[socket.room].m2,
              m3: io.sockets.adapter.rooms[socket.room].m3,
              m4: io.sockets.adapter.rooms[socket.room].m4
          });
      }
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    // echo globally that this client has left
    var ip = socket.handshake.address;
    ip = ip.replace('::ffff:', '');
    var numUsers = 0;
    var tableNumUsers = 0;
    var users = [];
    if(io.sockets.adapter.rooms[socket.room] != undefined) {
      var index = io.sockets.adapter.rooms[socket.room].ips.indexOf(ip);
      if (index > -1) {
        io.sockets.adapter.rooms[socket.room].ips.splice(index, 1);
      }
      var index = io.sockets.adapter.rooms[socket.room].disconnecteds.indexOf(socket.username);
      if (index == -1) {
        io.sockets.adapter.rooms[socket.room].disconnecteds.push(socket.username);
      }
      tableNumUsers = io.sockets.adapter.rooms[socket.room].tableUsers.length;
      numUsers = io.sockets.adapter.rooms[socket.room].users.length;
      users = io.sockets.adapter.rooms[socket.room].users;
        var index = io.sockets.adapter.rooms[socket.room].tableUsers.indexOf(socket.username);
        if (index > -1) {
            io.sockets.adapter.rooms[socket.room].tableUsers.splice(index, 1);
            //update number of table users
        }
    }
    io.sockets.in(socket.room).emit('user left', {
      username: socket.username,
      numUsers: numUsers,
      tableNumUsers : tableNumUsers
    });
    io.sockets.in(socket.room).emit('update chat', users, socket.username + ' از اتاق خارج شد');
    console.log('user : ' + socket.username + ' left room : ' + socket.room);

  });
});
