$(function() {
  var FADE_TIME = 150; // ms
  var TYPING_TIMER_LENGTH = 400; // ms
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
  ];
  var timer1;
  var timer2;
  var sec;

  var cards = [];
  // Initialize variables
  var $window = $(window);
  var $usernameInput = $('.usernameInput'); // Input for username
  var $messages = $('.messages'); // Messages area
  var $inputMessage = $('.inputMessage'); // Input message input box

  var $loginPage = $('.login.page'); // The login page
  var $chatPage = $('.chat.page'); // The chatroom page

  // Prompt for setting a username
  var username;
  var connected = false;
  var typing = false;
  var lastTypingTime;
  var $currentInput = $usernameInput.focus();

  var socket = io();
  var cards = [];

  function addParticipantsMessage (data) {
    var message = '';
    if (data.numUsers === 1) {
      message += "there's 1 participant";
    } else {
      message += "there are " + data.numUsers + " participants";
    }
    log(message);
  }

  // user varede miz shod
  function userLoginRoom () {
    connected = true;
    var userid = $('#userid').val();
    var user = $('#user').val();
    var room = $('#room').val();
    var game_limit = $('#game_limit').val();
    var time_limit = $('#time_limit').val();
    $('#first_player_m').html('');
    $('#second_player_m').html('');
    $('#third_player_m').html('');
    $('#fourth_player_m').html('');
    $('.cards').remove()
    cards = [];
    if(user != undefined && room != '') {
      socket.emit('user login room', userid, user, room, game_limit, time_limit);
    }
  }

  // Sends a chat message
  function sendMessage (msg) {
    //var message = $inputMessage.val();
    var message = msg;
    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', message);
    }
  }

  // Log a message
  function log (message, options) {
    var $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
  }

  // Adds the visual chat message to the message list
  function addChatMessage (data, options) {
    $('#messages').append(data + '<br>');
    $("#messages").animate({ scrollTop: $('#messages').prop("scrollHeight")}, 1000);
  }

  // Adds the visual chat typing message
  function addChatTyping (data) {
    data.typing = true;
    data.message = 'is typing';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  function removeChatTyping (data) {
    getTypingMessages(data).fadeOut(function () {
      $(this).remove();
    });
  }

  // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options.fade - If the element should fade-in (default = true)
  // options.prepend - If the element should prepend
  //   all other messages (default = false)
  function addMessageElement (el, options) {
    /*
     var $el = $(el);

     // Setup default options
     if (!options) {
     options = {};
     }
     if (typeof options.fade === 'undefined') {
     options.fade = true;
     }
     if (typeof options.prepend === 'undefined') {
     options.prepend = false;
     }

     // Apply options
     if (options.fade) {
     $el.hide().fadeIn(FADE_TIME);
     }
     if (options.prepend) {
     $messages.prepend($el);
     } else {
     $messages.append($el);
     }
     $messages[0].scrollTop = $messages[0].scrollHeight;
     */
  }

  // Prevents input from having injected markup
  function cleanInput (input) {
    return $('<div/>').text(input).text();
  }

  // Updates the typing event
  function updateTyping () {
    if (connected) {
      if (!typing) {
        typing = true;
        socket.emit('typing');
      }
      lastTypingTime = (new Date()).getTime();

      setTimeout(function () {
        var typingTimer = (new Date()).getTime();
        var timeDiff = typingTimer - lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
          socket.emit('stop typing');
          typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    }
  }

  // Gets the 'X is typing' messages of a user
  function getTypingMessages (data) {
    return $('.typing.message').filter(function (i) {
      return $(this).data('username') === data.username;
    });
  }

  // Gets the color of a username through our hash function
  function getUsernameColor (username) {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
      hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  $('body').on('click', '.set_hokm', function(){
    clearInterval(timer2);
    sendMessage('h:' + $(this).attr("value"));
    $('#set_hokm').html('');
  });

  $('body').on('click', '.card', function(){
    clearInterval(timer1);
    if($('#first_player_m').html() != '' && $('#second_player_m').html() != '' && $('#third_player_m').html() != '' && $('#fourth_player_m').html() != '') {
      $('#first_player_m').html('');
      $('#second_player_m').html('');
      $('#third_player_m').html('');
      $('#fourth_player_m').html('');
    }
    $('#cd').remove();
    sendMessage('m:' + $(this).attr('id'));
  });

  $('.set_place').click(function(){
    var user = $('#user').val();
    $(this).prop('disabled', true);
    switch ($(this).val()){
      case "1":
        $('.first_player').html(user);
        break;
      case "2":
        $('.second_player').html(user);
        break;
      case "3":
        $('.third_player').html(user);
        break;
      case "4":
        $('.fourth_player').html(user);
        break;
    }
    setUsername(user);
  });

  $('#ready_play').click(function(){
    var user = $('#user').val();
    var chair = $(this).val();
    $(this).remove();
    $('.fourth_player').html(user);
    socket.emit('user seat', user, chair);
  });

  // Keyboard events
  $window.keydown(function (event) {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      if (username) {
        sendMessage();
        socket.emit('stop typing');
        typing = false;
      } else {
        //setUsername();
      }
    }
  });

  $inputMessage.on('input', function() {
    updateTyping();
  });

  // Click events

  // Focus input when clicking anywhere on login page
  $loginPage.click(function () {
    $currentInput.focus();
  });

  // Focus input when clicking on the message input's border
  $inputMessage.click(function () {
    $inputMessage.focus();
  });

  // Socket events

  // Whenever the server emits 'login', log the login message
  socket.on('login', function (data) {
    connected = true;
    // Display the welcome message
    var message = "Welcome to Socket.IO Chat – ";
    log(message, {
      prepend: true
    });
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'new message', update the chat body
/*
  socket.on('new hand', function (data) {
    $('#cd').remove();
    var str = '';
    var sum13 = parseInt(data.user1score) + parseInt(data.user3score);
    var data.team2score = parseInt(data.user2score) + parseInt(data.user4score);
    str += data.user1 + ' - ' + data.user3 + ' : ' + data.setwinner13 + ' - ' + sum13 + '<br>';
    str += data.user2 + ' - ' + data.user4 + ' : ' + data.setwinner24 + ' - ' + data.team2score + '<br>';
    //str += 'نوبت : ' + data.nobat + '<br>';
    $('.seat').removeClass('blinking');
    $('#cd').remove();
    $('#' + data.nobat).addClass('blinking').append('<span id="cd"></span>');

    switch(data.hokm) {
      case 'hearts':
        str+= 'حکم : &hearts;';
        break;
      case 'spades':
        str+= 'حکم : &spades;';
        break;
      case 'diamonds':
        str+= 'حکم : &diams;';
        break;
      case 'clubs':
        str+= 'حکم : &clubs;';
        break;
    }
    $('#game_res').html(str);

    if(data.step == 1){
      $('#first_player_m').html('');
      $('#second_player_m').html('');
      $('#third_player_m').html('');
      $('#fourth_player_m').html('');
    } else if (data.step == 4 && move_flag) {
      setTimeout(function () {
        $('#first_player_m').html('');
        $('#second_player_m').html('');
        $('#third_player_m').html('');
        $('#fourth_player_m').html('');
      }, 10000);
    }

    if(data.card != undefined && data.card != 0) {
      var l = data.card * 46;
      switch ($('#' + data.username).attr('class')) {
        case 'seat first_player':
          $('#first_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
          break;
        case 'seat second_player':
          $('#second_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
          break;
        case 'seat third_player':
          $('#third_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
          break;
        case 'seat fourth_player':
          $('#fourth_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
          break;
      }
      $('#' + data.card).remove();
    }
    move_flag = false;
    rnd = 0;
    var new_move = 0;
    if(user == data.nobat) {
      move_flag = true;
      var sec = data.sec;
      cancel_timer1 = false;
      var timer1 = setInterval(function() {
        if(cancel_timer1){
          clearInterval(timer1);
        } else {
          $('#cd').text(' : ' + sec--);
          if (sec == -1) {
            $('#cd').remove();
            clearInterval(timer1);
            if (move_flag) {
              if (data.zamin == '') {
                rnd = Math.floor(Math.random() * $('#fourth_player_cards .card').length);
                $('#fourth_player_cards .card').eq(rnd).click();
              } else {
                var j = [];
                for (i = 0; i <= $('#fourth_player_cards .card').length; i++) {
                  if ($('#fourth_player_cards .card').eq(i).attr('id') >= data.minzamin && $('#fourth_player_cards .card').eq(i).attr('id') <= data.maxzamin) {
                    j.push($('#fourth_player_cards .card').eq(i).attr('id'));
                  }
                }
                if (j.length > 0) {
                  rnd = Math.floor(Math.random() * j.length);
                  $('#' + j[rnd]).click();
                } else {
                  rnd = Math.floor(Math.random() * $('#fourth_player_cards .card').length);
                  $('#fourth_player_cards .card').eq(rnd).click();
                }
              }
              $('#cd').remove();
            }
          }
        }
      }, 1000);
    }
  });
*/

  socket.on('new move', function (data) {
    $('#cd').remove();
    var user = $('#user').val();
    if(data.step == 1){
      $('#first_player_m').html('');
      $('#second_player_m').html('');
      $('#third_player_m').html('');
      $('#fourth_player_m').html('');
    } else if (data.step == 4) {
      setTimeout(function () {
        $('#first_player_m').html('');
        $('#second_player_m').html('');
        $('#third_player_m').html('');
        $('#fourth_player_m').html('');
      }, 10000);
    }
    var str = '';
    if(user == data.user1 || user == data.user3){
      str += '<div style="float:right;margin:2px;clear: both;">تیم شما : '+data.setwinner13+' - </div>';
      for(i=0;i<data.team1score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
      str+= '<br>';
      str += '<div style="float:right;margin:2px;clear: both;">تیم حریف : '+data.setwinner24+' - </div>';
      for(i=0;i<data.team2score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
    }
    if(user == data.user2 || user == data.user4) {
      str += '<div style="float:right;margin:2px;clear: both;">تیم شما : '+data.setwinner24+' - </div>';
      for(i=0;i<data.team2score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
      str+= '<br>';
      str += '<div style="float:right;margin:2px;clear: both;">تیم حریف : '+data.setwinner13+' - </div>';
      for(i=0;i<data.team1score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
    }
/*
    str += data.user1 + ' - ' + data.user3 + ' : ' + data.setwinner13 + ' - ' + data.team1score + ' *** ';
    str += data.user2 + ' - ' + data.user4 + ' : ' + data.setwinner24 + ' - ' + data.team2score + ' *** ';
*/
    //str += 'نوبت : ' + data.nobat + '<br>';
    $('.seat').removeClass('blinking');
    $('#cd').remove();
    if(user == data.nobat) {
      $('#' + data.nobat).addClass('blinking').append('<span id="cd"></span>');
    } else {
      $('#' + data.nobat).addClass('blinking');
    }
    str+= '<br><div style="float:right;margin:2px;clear:both;">';
    switch(data.hokm) {
      case 'hearts':
        str+= 'حکم : &hearts;';
        break;
      case 'spades':
        str+= 'حکم : &spades;';
        break;
      case 'diamonds':
        str+= 'حکم : &diams;';
        break;
      case 'clubs':
        str+= 'حکم : &clubs;';
        break;
    }
    str += '</div>';
    $('#game_res').html(str);

    if(data.team1score < 7 && data.team2score < 7) {
      var l = data.card * 46;
      if (data.card != undefined && data.card != 0) {
        switch ($('#' + data.username).attr('class')) {
          case 'seat first_player':
          case 'seat first_player blinking':
            $('#first_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
            break;
          case 'seat second_player':
          case 'seat second_player blinking':
            $('#second_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
            break;
          case 'seat third_player':
          case 'seat third_player blinking':
            $('#third_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
            break;
          case 'seat fourth_player':
          case 'seat fourth_player blinking':
            $('#fourth_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.card + '"></div>');
            break;
        }
        $('#' + data.card).remove();
      }
      rnd = 0;
      var new_move = 0;
      if (user == data.nobat) {
        sec = data.sec;
        timer1 = setInterval(function () {
          $('#cd').text(' : ' + sec--);
          if (sec < 0) {
            $('#cd').remove();
            clearInterval(timer1);
            if (data.zamin == '') {
              var s = [];
              var s2 = [];
              for (i = 0; i <= $('#fourth_player_cards .card').length; i++) {
                if ($('#fourth_player_cards .card').eq(i).attr('id') == 13 ||
                    $('#fourth_player_cards .card').eq(i).attr('id') == 26 ||
                    $('#fourth_player_cards .card').eq(i).attr('id') == 39 ||
                    $('#fourth_player_cards .card').eq(i).attr('id') == 52
                ) {
                  s.push($('#fourth_player_cards .card').eq(i).attr('id'));
                }
                if ($('#fourth_player_cards .card').eq(i).attr('id') == 12 ||
                    $('#fourth_player_cards .card').eq(i).attr('id') == 25 ||
                    $('#fourth_player_cards .card').eq(i).attr('id') == 38 ||
                    $('#fourth_player_cards .card').eq(i).attr('id') == 51
                ) {
                  s2.push($('#fourth_player_cards .card').eq(i).attr('id'));
                }
              }
              if (s.length > 0) {
                rnd = Math.floor(Math.random() * s.length);
                $('#' + s[rnd]).click();
              } else if (s2.length > 0 && $('#fourth_player_cards .card').length <= 9) {
                rnd = Math.floor(Math.random() * s2.length);
                $('#' + s2[rnd]).click();
              } else {
                rnd = Math.floor(Math.random() * $('#fourth_player_cards .card').length);
                $('#fourth_player_cards .card').eq(rnd).click();
              }
            } else {
              var j = [];
              for (i = 0; i <= $('#fourth_player_cards .card').length; i++) {
                if ($('#fourth_player_cards .card').eq(i).attr('id') >= data.minzamin && $('#fourth_player_cards .card').eq(i).attr('id') <= data.maxzamin) {
                  j.push($('#fourth_player_cards .card').eq(i).attr('id'));
                }
              }
              if (j.length > 0) {
                j.sort(function (a, b) {
                  return b - a
                });
                if (data.step == 2) {
                  $('#' + j[0]).click();
                } else {
                  $('#' + j[j.length - 1]).click();
                }
              } else {
                var h = [];
                for (i = 0; i <= $('#fourth_player_cards .card').length; i++) {
                  if ($('#fourth_player_cards .card').eq(i).attr('id') >= data.minhokm && $('#fourth_player_cards .card').eq(i).attr('id') <= data.maxhokm) {
                    h.push($('#fourth_player_cards .card').eq(i).attr('id'));
                  }
                }
                if (h.length > 0) {
                  h.sort(function (a, b) {
                    return b - a
                  });
                  if (data.step == 2) {
                    $('#' + h[0]).click();
                  } else {
                    $('#' + h[h.length - 1]).click();
                  }
                } else {
                  rnd = Math.floor(Math.random() * $('#fourth_player_cards .card').length);
                  $('#fourth_player_cards .card').eq(rnd).click();
                }
              }
            }
            $('#cd').remove();
          }
        }, 1000);
      }
    }
  });
  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', function (data) {
    addChatMessage(data.username + ':' + data.message);
  });

  socket.on('update chat', function (users, data) {
    $('#users').html('');
    $.each(users, function(i, user) {
      $('#users').append(i+1 + ' - ' + user + '<br>');
    })
    $("#users").animate({ scrollTop: $('#users').prop("scrollHeight")}, 1000);
    addChatMessage(data);
  });

  socket.on('set hand', function (data) {
/*
    $('#first_player_m').html('');
    $('#second_player_m').html('');
    $('#third_player_m').html('');
    $('#fourth_player_m').html('');
*/
    $('#fourth_player_cards .card').remove();

    cards = [];
    var crds = data;
    $.each(crds, function(i, card) {
      if(jQuery.inArray(card, cards) === -1)
        cards.push(card);
    })
    //sort cards
    cards.sort(function(a, b){return b-a});
    var l = 0;
    $.each(cards, function(i, card) {
      l = card * 46;
      $('#fourth_player_cards').append('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" id="' + card + '" value="' + card + '"></div>');
    })
  });

  socket.on('select hakem', function (data) {
    $('#cd').remove();
    var user = $('#user').val();
    if(user == data.user1){
      $('.first_player').text(data.user3);
      $('.second_player').text(data.user2);
      $('.third_player').text(data.user4);
      $('.first_player').attr('id', data.user3);
      $('.second_player').attr('id', data.user2);
      $('.third_player').attr('id', data.user4);
      $('.fourth_player').attr('id', data.user1);
    }
    if(user == data.user2){
      $('.first_player').text(data.user4);
      $('.second_player').text(data.user3);
      $('.third_player').text(data.user1);
      $('.first_player').attr('id', data.user4);
      $('.second_player').attr('id', data.user3);
      $('.third_player').attr('id', data.user1);
      $('.fourth_player').attr('id', data.user2);
    }
    if(user == data.user3){
      $('.first_player').text(data.user1);
      $('.second_player').text(data.user4);
      $('.third_player').text(data.user2);
      $('.first_player').attr('id', data.user1);
      $('.second_player').attr('id', data.user4);
      $('.third_player').attr('id', data.user2);
      $('.fourth_player').attr('id', data.user3);
    }
    if(user == data.user4){
      $('.first_player').text(data.user2);
      $('.second_player').text(data.user1);
      $('.third_player').text(data.user3);
      $('.first_player').attr('id', data.user2);
      $('.second_player').attr('id', data.user1);
      $('.third_player').attr('id', data.user3);
      $('.fourth_player').attr('id', data.user4);
    }
    $('#first_player_hakem').text('');
    $('#second_player_hakem').text('');
    $('#third_player_hakem').text('');
    $('#fourth_player_hakem').text('');

    $('.set_hokm').remove();
    $('#set_hokm').html('');
    if(user == data.hakem) {
      $('#set_hokm').append('<span>حکم خود را انتخاب کنید : </span>');
      $('#set_hokm').append('<button class="btn btn-danger set_hokm" id="set_hokm_hearts" value="h">&hearts;</button>');
      $('#set_hokm').append('<button class="btn btn-success set_hokm" id="set_hokm_spades" value="s">&spades;</button>');
      $('#set_hokm').append('<button class="btn btn-danger set_hokm" id="set_hokm_diamonds" value="d">&diams;</button>');
      $('#set_hokm').append('<button class="btn btn-success set_hokm" id="set_hokm_clubs" value="c">&clubs;</button>');
      var l = 0;
      $('#fourth_player_cards .card').remove();
      $.each(cards, function(i, card) {
        l = card * 46;
        $('#fourth_player_cards').append('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" id="' + card + '" value="' + card + '"></div>');
      })
    }
    var x = '';
    $.each($('.seat'), function(i, user) {
      if($(user).html() != undefined) {
        x = $(user).html().replace('♔<br>', '');
        $(user).html(x);
      }
    })
    if($('#' + data.hakem).html() != undefined) {
      x = $('#' + data.hakem).html().replace('♔<br>', '');
      $('#' + data.hakem).html('♔<br>' + x);
    }
    $('.seat').removeClass('blinking');
    $('#cd').remove();
    $('#' + data.nobat).addClass('blinking').append('<span id="cd"></span>');

    rnd = 0;
    var new_move = 0;
    if(user == data.nobat) {
      sec = data.sec;
      timer2 = setInterval(function() {
        $('#cd').text(' : ' + sec--);
        if (sec < 0) {
          $('#cd').remove();
          clearInterval(timer2);
          var ho = '';
          var h = 0;
          var s = 0;
          var d = 0;
          var c = 0;
          var hw = 0;
          var sw = 0;
          var dw = 0;
          var cw = 0;
          for (i = 0; i <= $('#fourth_player_cards .card').length; i++) {
            if ($('#fourth_player_cards .card').eq(i).attr('id') >= 1 && $('#fourth_player_cards .card').eq(i).attr('id') <= 13) {
              h++;
              hw += parseInt($('#fourth_player_cards .card').eq(i).attr('id'));
            }
            if ($('#fourth_player_cards .card').eq(i).attr('id') >= 14 && $('#fourth_player_cards .card').eq(i).attr('id') <= 26) {
              s++;
              sw += parseInt($('#fourth_player_cards .card').eq(i).attr('id')) - 13;
            }
            if ($('#fourth_player_cards .card').eq(i).attr('id') >= 27 && $('#fourth_player_cards .card').eq(i).attr('id') <= 39) {
              d++;
              dw += parseInt($('#fourth_player_cards .card').eq(i).attr('id')) - 26;
            }
            if ($('#fourth_player_cards .card').eq(i).attr('id') >= 40 && $('#fourth_player_cards .card').eq(i).attr('id') <= 52) {
              c++;
              cw += parseInt($('#fourth_player_cards .card').eq(i).attr('id')) - 39;
            }
          }
          var maxh = h;
          var maxhw = hw;
          ho = 'set_hokm_hearts';
          if ((s > maxh) || (s == maxh && sw > maxhw)) {
            ho = 'set_hokm_spades';
            maxh = s;
            maxhw = sw;
          }
          if ((d > maxh) || (d == maxh && dw > maxhw)) {
            ho = 'set_hokm_diamonds';
            maxh = d;
            maxhw = dw;
          }
          if ((c > maxh) || (c == maxh && cw > maxhw)) {
            ho = 'set_hokm_clubs';
            maxh = c;
            maxhw = cw;
          }
          $('#' + ho).click();
          $('#cd').remove();
        }
      }, 1000);
    }
  });

  socket.on('set username', function (data) {
    $('#username').html(data);
  });
  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('guest view', function (data) {
    var user = $('#user').val();
    var str = '';
    if(user == data.user1 || user == data.user3){
      str += '<div style="float:right;margin:2px;clear: both;">تیم شما : '+data.setwinner13+' - </div>';
      for(i=0;i<data.team1score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
      str+= '<br>';
      str += '<div style="float:right;margin:2px;clear: both;">تیم حریف : '+data.setwinner24+' - </div>';
      for(i=0;i<data.team2score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
    }
    if(user == data.user2 || user == data.user4) {
      str += '<div style="float:right;margin:2px;clear: both;">تیم شما : '+data.setwinner24+' - </div>';
      for(i=0;i<data.team2score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
      str+= '<br>';
      str += '<div style="float:right;margin:2px;clear: both;">تیم حریف : '+data.setwinner13+' - </div>';
      for(i=0;i<data.team1score;i++){
        str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;float:right;margin:2px;"></div>';
      }
    }
    //str += data.user1 + ' - ' + data.user3 + ' : ' + data.setwinner13 + ' - ' + data.team1score + ' *** ';
    //str += data.user2 + ' - ' + data.user4 + ' : ' + data.setwinner24 + ' - ' + data.team2score + ' *** ';
    //str += 'نوبت : ' + data.nobat + '<br>';
    $('.seat').removeClass('blinking');
    $('#cd').remove();
    $('#' + data.nobat).addClass('blinking').append('<span id="cd"></span>');
    str+= '<br><div style="float:right;margin:2px;clear:both;">';
    switch(data.hokm) {
      case 'hearts':
        str+= 'حکم : &hearts;';
        break;
      case 'spades':
        str+= 'حکم : &spades;';
        break;
      case 'diamonds':
        str+= 'حکم : &diams;';
        break;
      case 'clubs':
        str+= 'حکم : &clubs;';
        break;
    }
    str += '</div>';
    $('#game_res').html(str);

    if(user == data.user1){
      $('.first_player').text(data.user3);
      $('.second_player').text(data.user2);
      $('.third_player').text(data.user4);
      $('.fourth_player').text(data.user1);
      $('.first_player').attr('id', data.user3);
      $('.second_player').attr('id', data.user2);
      $('.third_player').attr('id', data.user4);
      $('.fourth_player').attr('id', data.user1);
    } else if(user == data.user2){
      $('.first_player').text(data.user4);
      $('.second_player').text(data.user3);
      $('.third_player').text(data.user1);
      $('.fourth_player').text(data.user2);
      $('.first_player').attr('id', data.user4);
      $('.second_player').attr('id', data.user3);
      $('.third_player').attr('id', data.user1);
      $('.fourth_player').attr('id', data.user2);
    } else if(user == data.user3){
      $('.first_player').text(data.user1);
      $('.second_player').text(data.user4);
      $('.third_player').text(data.user2);
      $('.fourth_player').text(data.user3);
      $('.first_player').attr('id', data.user1);
      $('.second_player').attr('id', data.user4);
      $('.third_player').attr('id', data.user2);
      $('.fourth_player').attr('id', data.user3);
    } else if(user == data.user4){
      $('.first_player').text(data.user2);
      $('.second_player').text(data.user1);
      $('.third_player').text(data.user3);
      $('.fourth_player').text(data.user4);
      $('.first_player').attr('id', data.user2);
      $('.second_player').attr('id', data.user1);
      $('.third_player').attr('id', data.user3);
      $('.fourth_player').attr('id', data.user4);
    } else {
      $('.first_player').text(data.user1);
      $('.fourth_player').text(data.user3);
      $('.second_player').text(data.user2);
      $('.third_player').text(data.user4);
      $('.first_player').attr('id', data.user1);
      $('.fourth_player').attr('id', data.user3);
      $('.second_player').attr('id', data.user2);
      $('.third_player').attr('id', data.user4);
    }
    $('#first_player_hakem').text('');
    $('#second_player_hakem').text('');
    $('#third_player_hakem').text('');
    $('#fourth_player_hakem').text('');

    if($('#' + data.hakem).html() != undefined) {
      x = $('#' + data.hakem).html().replace('♔<br>', '');
      $('#' + data.hakem).html('♔<br>' + x);
    }
    $('.seat').removeClass('blinking');
    $('#cd').remove();
    $('#' + data.nobat).addClass('blinking').append('<span id="cd"></span>');

    if(data.m1 != 0 && data.m1 != undefined) {
      var l = data.m1 * 46;
      switch ($('#' + data.user1).attr('class')) {
        case 'seat first_player':
          $('#first_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m1 + '"></div>');
          break;
        case 'seat second_player':
          $('#second_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m1 + '"></div>');
          break;
        case 'seat third_player':
          $('#third_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m1 + '"></div>');
          break;
        case 'seat fourth_player':
          $('#fourth_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m1 + '"></div>');
          break;
      }
    }

    if(data.m2 != 0 && data.m2 != undefined) {
      var l = data.m2 * 46;
      switch ($('#' + data.user2).attr('class')) {
        case 'seat first_player':
          $('#first_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m2 + '"></div>');
          break;
        case 'seat second_player':
          $('#second_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m2 + '"></div>');
          break;
        case 'seat third_player':
          $('#third_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m2 + '"></div>');
          break;
        case 'seat fourth_player':
          $('#fourth_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m2 + '"></div>');
          break;
      }
    }

    if(data.m3 != 0 && data.m3 != undefined) {
      var l = data.m3 * 46;
      switch ($('#' + data.user3).attr('class')) {
        case 'seat first_player':
          $('#first_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m3 + '"></div>');
          break;
        case 'seat second_player':
          $('#second_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m3 + '"></div>');
          break;
        case 'seat third_player':
          $('#third_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m3 + '"></div>');
          break;
        case 'seat fourth_player':
          $('#fourth_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m3 + '"></div>');
          break;
      }
    }

    if(data.m4 != 0 && data.m4 != undefined) {
      var l = data.m4 * 46;
      switch ($('#' + data.user4).attr('class')) {
        case 'seat first_player':
          $('#first_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m4 + '"></div>');
          break;
        case 'seat second_player':
          $('#second_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m4 + '"></div>');
          break;
        case 'seat third_player':
          $('#third_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m4 + '"></div>');
          break;
        case 'seat fourth_player':
          $('#fourth_player_m').html('<div class="card" style="background: url(img/cards.png) no-repeat -' + l + 'px 0px;" value="' + data.m4 + '"></div>');
          break;
      }
    }
    $('#set_hokm').html('');
    $('#ready_play').remove();
  });

  socket.on('game finish', function (data) {
    $('#info').append('<a href="/?uid=' + $('#userid').val() + '&uname=' + $('#user').val() + '&room=' + $('#room').val() + '&gl=' + $('#game_limit').val() + '&si=' + $('#si').val() + '&retry=1 " id="retry_play" class="btn btn-danger">بازی مجدد</a>');
    var str = '';
    if(user == data.user1 || user == data.user3){
      str += '<div style="float:right;margin:2px;clear: both;">تیم شما : '+data.setwinner13+' </div>';
      str+= '<br>';
      str += '<div style="float:right;margin:2px;clear: both;">تیم حریف : '+data.setwinner24+' </div>';
    }
    if(user == data.user2 || user == data.user4) {
      str += '<div style="float:right;margin:2px;clear: both;">تیم شما : '+data.setwinner24+' </div>';
      str+= '<br>';
    }
    //str += data.user1 + ' : ' + data.user1score + ' - ' + data.user3 + ' : ' + data.user3score + ' => ' + data.setwinner13 + ' *** ';
    //str += data.user2 + ' : ' + data.user2score + ' - ' + data.user4 + ' : ' + data.user4score + ' => ' + data.setwinner24 + ' *** ';
/*
    if($('#userid').val() == data.user1 || $('#userid').val() == data.user3){
      str+= 'تیم شما : ';
      for(i=0;i<data.team1score;i++){
 str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;"></div>';
      }
    }
    str+= '<br>';
    if($('#userid').val() == data.user1 || $('#userid').val() == data.user3) {
      str += 'تیم حریف : ';
      for(i=0;i<data.team2score;i++){
 str += '<div style="background: url(img/win.png) no-repeat 0px 0px;width: 11px; height: 15px;"></div>';
      }
    }
*/
    str+= '<br>';
    str += 'تیم برنده : ' + data.gamewinner;
    $('#set_info').append(str);
    $('#game_res').html(str);
    setTimeout(function () {
      $('.card').remove();
      $('#first_player_m').html('');
      $('#second_player_m').html('');
      $('#third_player_m').html('');
      $('#fourth_player_m').html('');
    }, 10000);
  });
  socket.on('user seat', function (user) {
    switch (user.chair){
      case '1':
        $('.first_player').html(user.username);
        $('.first_player').prop('disabled', true);
        break;
      case '2':
        $('.second_player').html(user.username);
        $('.second_player').prop('disabled', true);
        break;
      case '3':
        $('.third_player').html(user.username);
        $('.third_player').prop('disabled', true);
        break;
      case '4':
        $('.fourth_player').html(user.username);
        $('.fourth_player').prop('disabled', true);
        break;
    }
    addChatMessage(user.username + ' وارد بازی شد');
    if(user.username == $('#user').val())
      $('#ready_play').remove();
  });

  // Whenever the server emits 'user left', log it in the chat body
  socket.on('user left', function (data) {
    log(data.username + ' left');
    addParticipantsMessage(data);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', function (data) {
    addChatTyping(data);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', function (data) {
    removeChatTyping(data);
  });

  socket.on('disconnect', function () {
    log('you have been disconnected');
  });

  socket.on('reconnect', function () {
    log('you have been reconnected');
    if (username) {
      socket.emit('add user', username);
    }
  });

  socket.on('reconnect_error', function () {
    log('attempt to reconnect has failed');
  });
  userLoginRoom();
  setInterval(function() {
    socket.emit('new message', 'ack');
  }, 30000);
});